﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Escape.Data.CalendarEnigma
{
    // SOLUTION DE L'ENIGME CALENDAR (il n'y a pas d'objet "Calendar" car la génération du calendrier se fait a sa création)
    public class CalendarSolution
    {
        public int Id { get; set; }// <-- ID de la solution
        public int IdEnigma { get; set; }// <-- Enigme correspondante

        // Date de la solution:
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        // Evenement de la solution:
        public string Event { get; set; }// <-- String a ajouter a la date de la solution pour gagner
    }
}
