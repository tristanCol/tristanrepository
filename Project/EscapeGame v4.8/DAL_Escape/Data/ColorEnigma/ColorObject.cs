﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Escape.Data.ColorEnigma
{
    // OBJET DE L'ENIGME COLOR
    public class ColorObject
    {
        public int Id { get; set; }// <-- ID de l'objet
        public int IdEnigma { get; set; }// <-- Enigme correspondante
        public string Color { get; set; }// <-- Couleur du "carré"
    }
}
