﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Escape.Data.BookEnigma
{
    // OBJET POUR L'ENIGME BOOK
    public class BookObject
    {
        public int Id { get; set; }// <-- ID de l'objet
        public int IdEnigma { get; set; }// <-- Enigme correspondante
        public string Titre { get; set; }// <-- Titre a placer au bon endroit dans la liste

        // PAS UTILE POUR L'ENIGME
        public string Auteur { get; set; }
        public int NbPages { get; set; }
    }
}
