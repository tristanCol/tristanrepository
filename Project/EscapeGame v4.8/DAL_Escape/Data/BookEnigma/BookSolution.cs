﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Escape.Data.BookEnigma
{
    // SOLUTION DE L'ENIGME BOOK
    public class BookSolution
    {
        public int Id { get; set; }// <-- ID de la solution
        public int IdEnigma { get; set; }// <-- Enigme correspondante
        public string Solution { get; set; }// <-- Concaténation des "Titre" de chaque "BookObject" dans le bon ordre
    }
}
