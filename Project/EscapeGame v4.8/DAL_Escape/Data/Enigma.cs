﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Escape.Data
{
    // CLASSE ENIGME
    public class Enigma
    {
        public int Id { get; set; }// <-- ID de l'énigme
        public string Name { get; set; }// <-- Nom de l'énigme
    }
}
