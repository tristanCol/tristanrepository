﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Escape.Data
{
    // SOLUTION DE L'ENIGME BASKET
    public class BasketSolution
    {
        public int Id { get; set; }// <-- ID de la solution
        public int IdEnigma { get; set; }// <-- Enigme correspondante
        public string Solution { get; set; }// <-- Concaténation des "Word" de chaque "BasketObject" dans le bon ordre

    }
}
