﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Escape.Data
{
    // OBJET POUR L'ENIGME BASKET
    public class BasketObject
    {
        public int Id { get; set; } // <-- ID de l'objet
        public int IdEnigma { get; set; }// <-- Enigme correspondante
        public string Word { get; set; }// <-- Mot a placer au bon endroit dans la liste
    }
}
