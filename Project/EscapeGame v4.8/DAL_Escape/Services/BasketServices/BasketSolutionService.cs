﻿using DAL_Escape.Data;
using DAL_Escape.Mapper;
using DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Escape.Services
{
    // SERVICES D'APPEL DE LA DB POUR "BasketSolution"
    public class BasketSolutionService : IRepository<BasketSolution, int>
    {
        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<BasketSolution> Get()
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM BasketSolution", false);
            return connection.ExecuteReader<BasketSolution>(command, reader => reader.ToBasketSolution());
        }

        public BasketSolution Get(int id)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM BasketSolution WHERE IdBasketSolution = @id", false);
            command.AddParameter("id", id);
            return connection.ExecuteReader<BasketSolution>(command, reader => reader.ToBasketSolution()).SingleOrDefault();
        }

        public int Insert(BasketSolution entity)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("INSERT INTO BasketSolution (IdEnigma, Solution) OUTPUT inserted.IdBasketSolution values (@IdEnigma, @Solution)", false);

            command.AddParameter("IdEnigma", entity.Id);
            command.AddParameter("Solution", entity.Solution);
            return (int)connection.ExecuteScalar(command);
        }

        public bool Update(BasketSolution entity)
        {
            throw new NotImplementedException();
        }
    }
}
