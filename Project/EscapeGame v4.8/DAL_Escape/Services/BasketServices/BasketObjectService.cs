﻿using DAL_Escape.Data;
using DAL_Escape.Mapper;
using DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Escape.Services
{
    // SERVICES D'APPEL DE LA DB POUR "BasketObject"
    public class BasketObjectService : IRepository<BasketObject, int>
    {
        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<BasketObject> Get()
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM BasketObject", false);
            return connection.ExecuteReader<BasketObject>(command, reader => reader.ToBasketObject());
        }

        public BasketObject Get(int id)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM BasketObject WHERE IdBasketObject = @id", false);
            command.AddParameter("id", id);
            return connection.ExecuteReader<BasketObject>(command, reader => reader.ToBasketObject()).SingleOrDefault();
        }

        public int Insert(BasketObject entity)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("INSERT INTO BasketObject (IdEnigma, Word) OUTPUT inserted.Id BasketObject values (@IdEnigma, @Word)", false);
            
            command.AddParameter("IdEnigma", entity.IdEnigma);
            command.AddParameter("Word", entity.Word);
            return (int)connection.ExecuteScalar(command);
        }

        public bool Update(BasketObject entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<BasketObject> GetByEnigma(int id)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM BasketObject WHERE IdEnigma = @id", false);
            command.AddParameter("id", id);
            return connection.ExecuteReader<BasketObject>(command, reader => reader.ToBasketObject());
        }
    }
}
