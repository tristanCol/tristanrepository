﻿using DAL_Escape.Data.ColorEnigma;
using DAL_Escape.Mapper;
using DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Escape.Services.ColorServices
{
    // SERVICES D'APPEL DE LA DB POUR "ColorObject"
    public class ColorObjectService : IRepository<ColorObject, int>
    {
        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ColorObject> Get()
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM ColorObject", false);
            return connection.ExecuteReader<ColorObject>(command, reader => reader.ToColorObject());
        }

        public ColorObject Get(int id)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM ColorObject WHERE IdColorObject = @id", false);
            command.AddParameter("id", id);
            return connection.ExecuteReader<ColorObject>(command, reader => reader.ToColorObject()).SingleOrDefault();
        }

        public int Insert(ColorObject entity)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("INSERT INTO ColorObject (IdEnigma, Color) " +
                "OUTPUT inserted.IdColorObject values (@IdEnigma, @Color)", false);

            command.AddParameter("IdEnigma", entity.IdEnigma);
            command.AddParameter("Color", entity.Color);
            return (int)connection.ExecuteScalar(command);
        }

        public bool Update(ColorObject entity)
        {
            throw new NotImplementedException();
        }
    }
}
