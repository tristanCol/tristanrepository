﻿using DAL_Escape.Data.ColorEnigma;
using DAL_Escape.Mapper;
using DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Escape.Services.ColorServices
{
    // SERVICES D'APPEL DE LA DB POUR "ColorSolution"
    public class ColorSolutionService : IRepository<ColorSolution, int>
    {
        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ColorSolution> Get()
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM ColorSolution", false);
            return connection.ExecuteReader<ColorSolution>(command, reader => reader.ToColorSolution());
        }

        public ColorSolution Get(int id)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM ColorSolution WHERE IdColorSolution = @id", false);
            command.AddParameter("id", id);
            return connection.ExecuteReader<ColorSolution>(command, reader => reader.ToColorSolution()).SingleOrDefault();
        }

        public int Insert(ColorSolution entity)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("INSERT INTO ColorSolution (IdEnigma, Solution) " +
                "OUTPUT inserted.IdColorSolution values (@IdEnigma, @Solution)", false);

            command.AddParameter("IdEnigma", entity.IdEnigma);
            command.AddParameter("Solution", entity.Solution);
            return (int)connection.ExecuteScalar(command);
        }
    

        public bool Update(ColorSolution entity)
        {
            throw new NotImplementedException();
        }
    }
}
