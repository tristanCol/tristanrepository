﻿using DAL_Escape.Data.CalendarEnigma;
using DAL_Escape.Mapper;
using DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Escape.Services.CalendarServices
{
    // SERVICES D'APPEL DE LA DB POUR "CalendarSolution"
    public class CalendarSolutionService : IRepository<CalendarSolution, int>
    {
        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CalendarSolution> Get()
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM CalendarSolution", false);
            return connection.ExecuteReader<CalendarSolution>(command, reader => reader.ToCalendarSolution());
        }

        public CalendarSolution Get(int id)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM CalendarSolution WHERE IdCalendarSolution = @id", false);
            command.AddParameter("id", id);
            return connection.ExecuteReader<CalendarSolution>(command, reader => reader.ToCalendarSolution()).SingleOrDefault();
        }

        public int Insert(CalendarSolution entity)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("INSERT INTO CalendarSolution (IdEnigma, Day, Month, Year, Event) " +
                "OUTPUT inserted.IdCalendarSolution values (@IdEnigma, @Day, @Month, @Year, @Event)", false);

            command.AddParameter("IdEnigma", entity.IdEnigma);
            command.AddParameter("Day", entity.Day);
            command.AddParameter("Month", entity.Month);
            command.AddParameter("Year", entity.Year);
            command.AddParameter("Event", entity.Event);
            return (int)connection.ExecuteScalar(command);
        }

        public bool Update(CalendarSolution entity)
        {
            throw new NotImplementedException();
        }
    }
}
