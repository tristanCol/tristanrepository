﻿using DAL_Escape.Data;
using DAL_Escape.Mapper;
using DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Escape.Services
{
    // SERVICES D'APPEL DE LA DB POUR LES ENIGMES
    public class EnigmaService : IRepository<Enigma, int>
    {
        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Enigma> Get()
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM Enigma", false);
            return connection.ExecuteReader<Enigma>(command, reader => reader.ToEnigma());
        }

        public Enigma Get(int id)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM Enigma WHERE IdEnigma = @id", false);
            command.AddParameter("id", id);
            return connection.ExecuteReader<Enigma>(command, reader => reader.ToEnigma()).SingleOrDefault();
        }

        public int Insert(Enigma entity)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("INSERT INTO Enigma (EnigmaName) OUTPUT inserted.IdEnigma values (@Name)", false);
            command.AddParameter("Name", entity.Name);
            return (int)connection.ExecuteScalar(command);
        }

        public bool Update(Enigma entity)
        {
            throw new NotImplementedException();
        }
    }
}
