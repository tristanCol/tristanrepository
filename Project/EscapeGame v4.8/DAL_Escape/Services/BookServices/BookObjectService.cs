﻿using DAL_Escape.Data.BookEnigma;
using DAL_Escape.Mapper;
using DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Escape.Services.BookServices
{
    // SERVICES D'APPEL DE LA DB POUR "BookObject"
    public class BookObjectService : IRepository<BookObject, int>
    {
        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<BookObject> Get()
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM BookObject", false);
            return connection.ExecuteReader<BookObject>(command, reader => reader.ToBookObject());
        }

        public BookObject Get(int id)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM BookObject WHERE IdBookObject = @id", false);
            command.AddParameter("id", id);
            return connection.ExecuteReader<BookObject>(command, reader => reader.ToBookObject()).SingleOrDefault();
        }

        public int Insert(BookObject entity)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("INSERT INTO BookObject (IdEnigma, Titre, Auteur, NbPages) " +
                "OUTPUT inserted.IdBookObject values (@IdEnigma, @Titre, @Auteur, @NbPages)", false);

            command.AddParameter("IdEnigma", entity.IdEnigma);
            command.AddParameter("Titre", entity.Titre);
            command.AddParameter("Auteur", entity.Auteur);
            command.AddParameter("NbPages", entity.NbPages);
            return (int)connection.ExecuteScalar(command);
        }

        public bool Update(BookObject entity)
        {
            throw new NotImplementedException();
        }
    }
}
