﻿using DAL_Escape.Data.BookEnigma;
using DAL_Escape.Mapper;
using DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Escape.Services.BookServices
{
    // SERVICES D'APPEL DE LA DB POUR "BookSolution"
    public class BookSolutionService : IRepository<BookSolution, int>
    {
        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<BookSolution> Get()
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM BookSolution", false);
            return connection.ExecuteReader<BookSolution>(command, reader => reader.ToBookSolution());
        }

        public BookSolution Get(int id)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM BookSolution WHERE IdBookSolution = @id", false);
            command.AddParameter("id", id);
            return connection.ExecuteReader<BookSolution>(command, reader => reader.ToBookSolution()).SingleOrDefault();
        }

        public int Insert(BookSolution entity)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("INSERT INTO BookSolution (IdEnigma, Solution) " +
                "OUTPUT inserted.IdBookSolution values (@IdEnigma, @Solution)", false);

            command.AddParameter("IdEnigma", entity.IdEnigma);
            command.AddParameter("Solution", entity.Solution);
            return (int)connection.ExecuteScalar(command);
        }

        public bool Update(BookSolution entity)
        {
            throw new NotImplementedException();
        }
    }
}
