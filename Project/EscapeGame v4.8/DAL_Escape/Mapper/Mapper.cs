﻿using DAL_Escape.Data;
using DAL_Escape.Data.BookEnigma;
using DAL_Escape.Data.CalendarEnigma;
using DAL_Escape.Data.ColorEnigma;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Escape.Mapper
{
    // MAPPER POUR LA CONVERTION DES DONNEES DE LA DB EN OBJET DE LA DAL
    public static class Mapper
    {
        public static Enigma ToEnigma(this IDataRecord record) => new Enigma()
        {
            Id = (int)record["IdEnigma"],
            Name = (string)record["EnigmaName"]
        };

        public static BasketObject ToBasketObject(this IDataRecord record) => new BasketObject()
        {
            Id = (int)record["IdBasketObject"],
            IdEnigma = (int)record["IdEnigma"],
            Word = (string)record["Word"]
        };

        public static BasketSolution ToBasketSolution(this IDataRecord record) => new BasketSolution()
        {
            Id = (int)record["IdBasketSolution"],
            IdEnigma = (int)record["IdEnigma"],
            Solution = (string)record["Solution"]
        };

        public static BookObject ToBookObject(this IDataRecord record) => new BookObject()
        {
            Id = (int)record["IdBookObject"],
            IdEnigma = (int)record["IdEnigma"],
            Auteur = (string)record["Auteur"],
            Titre = (string)record["Titre"],
            NbPages = (int)record["NbPages"]
        };

        public static BookSolution ToBookSolution(this IDataRecord record) => new BookSolution()
        {
            Id = (int)record["IdBookSolution"],
            IdEnigma = (int)record["IdEnigma"],
            Solution = (string)record["Solution"]
        };

        public static ColorObject ToColorObject(this IDataRecord record) => new ColorObject()
        {
            Id = (int)record["IdColorObject"],
            IdEnigma = (int)record["IdEnigma"],
            Color = (string)record["Color"]
        };

        public static ColorSolution ToColorSolution(this IDataRecord record) => new ColorSolution()
        {
            Id = (int)record["IdColorSolution"],
            IdEnigma = (int)record["IdEnigma"],
            Solution = (string)record["Solution"]
        };

        public static CalendarSolution ToCalendarSolution(this IDataRecord record) => new CalendarSolution()
        {
            Id = (int)record["IdCalendarSolution"],
            IdEnigma = (int)record["IdEnigma"],
            Day = (int)record["Day"],
            Month = (int)record["Month"],
            Year = (int)record["Year"],
            Event = (string)record["Event"]
        };

    }
}
