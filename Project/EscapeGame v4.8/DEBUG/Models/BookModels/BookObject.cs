﻿using DEBUG.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DEBUG.Models.BookModels
{
    public class BookObject : ISelectable
    {
        public int Id { get; set; }
        public string Word { get; set; }
        public bool IsSelected { get; set; }

        //non utilisé
        public string Auteur { get; set; }
        public int NbPages { get; set; }
    }
}
