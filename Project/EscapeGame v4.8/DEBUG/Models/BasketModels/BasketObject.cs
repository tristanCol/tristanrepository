﻿using DEBUG.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DEBUG.Models
{
    public class BasketObject : ISelectable
    {
        public int Id { get; set; }
        public string Word { get; set; }
        public bool IsSelected { get; set; }
        public string Extention { get; }

        public BasketObject()
        {
            Thread.Sleep(10);
            Random rnd = new Random();
            int rng = rnd.Next(1, 7);// <-- Défini une extension de fichier aléatoire au objet de l'énigme
                                     //     (uniquement utile pour de l'affichage)

            switch (rng)
            {
                case 1:
                    Extention = "exe";
                    break;
                case 2:
                    Extention = "zip";
                    break;
                case 3:
                    Extention = "txt";
                    break;
                case 4:
                    Extention = "mp3";
                    break;
                case 5:
                    Extention = "pdf";
                    break;
                case 6:
                    Extention = "wav";
                    break;
            }
        }
    }
}
