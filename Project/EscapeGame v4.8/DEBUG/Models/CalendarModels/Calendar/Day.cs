﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEBUG.Models.CalendarModels.Calendar
{
    public enum NameDays
    {
        DIMANCHE,
        LUNDI,
        MARDI,
        MERCREDI,
        JEUDI,
        VENDREDI,
        SAMEDI
    }

    public class Day
    {
        public NameDays Name { get; set; }
        public string Event { get; set; }
        public bool IsSelected { get; set; }

        public Day(NameDays name) // CTOR
        {
            Name = name;
            IsSelected = false;
            Event = "";
        }
    }
}
