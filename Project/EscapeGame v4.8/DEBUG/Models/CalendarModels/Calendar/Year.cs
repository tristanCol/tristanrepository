﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEBUG.Models.CalendarModels.Calendar
{
    public class Year
    {
        public int IntYear { get; set; }
        public Dictionary<int, Month> Months { get; set; }

        public Year(int entier)
        {
            IntYear = entier;
            Months = new Dictionary<int, Month>();
            Month Month = null;
            for (int i = 1; i <= 12; i++)
            {
                switch (i)
                {
                    case 1:
                        Month = new Month(NameMonth.JANVIER, this);
                        break;
                    case 2:
                        Month = new Month(NameMonth.FEVRIER, this);
                        break;
                    case 3:
                        Month = new Month(NameMonth.MARS, this);
                        break;
                    case 4:
                        Month = new Month(NameMonth.AVRIL, this);
                        break;
                    case 5:
                        Month = new Month(NameMonth.MAI, this);
                        break;
                    case 6:
                        Month = new Month(NameMonth.JUIN, this);
                        break;
                    case 7:
                        Month = new Month(NameMonth.JUILLET, this);
                        break;
                    case 8:
                        Month = new Month(NameMonth.AOUT, this);
                        break;
                    case 9:
                        Month = new Month(NameMonth.SEPTEMBRE, this);
                        break;
                    case 10:
                        Month = new Month(NameMonth.OCTOBRE, this);
                        break;
                    case 11:
                        Month = new Month(NameMonth.NOVEMBRE, this);
                        break;
                    case 12:
                        Month = new Month(NameMonth.DECEMBRE, this);
                        break;
                }
                Months.Add(i, Month);
            }

        }
    }
}
