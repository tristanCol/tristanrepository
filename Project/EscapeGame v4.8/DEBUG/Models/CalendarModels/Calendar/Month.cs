﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEBUG.Models.CalendarModels.Calendar
{
    public enum NameMonth
    {
        JANVIER = 1,
        FEVRIER = 2,
        MARS = 3,
        AVRIL = 4,
        MAI = 5,
        JUIN = 6,
        JUILLET = 7,
        AOUT = 8,
        SEPTEMBRE = 9,
        OCTOBRE = 10,
        NOVEMBRE = 11,
        DECEMBRE = 12
    }

    public class Month
    {
        public NameMonth Name { get; set; }
        public int IntMonth { get; set; }
        public List<Day> Days { get; set; }
        public int NbDays { get; set; }

        public Month(NameMonth Month, Year Year) // CTOR
        {
            Name = Month;
            IntMonth = (int)Month;
            Days = new List<Day>();

            //nbDays
            if ((int)Month == 2 && (Year.IntYear % 4 == 0 && Year.IntYear % 100 != 0) || Year.IntYear % 400 == 0) // Year bissextile fevrier
            {
                NbDays = 29;

            }
            else if ((int)Month == 2) // fev
            {
                NbDays = 28;
            }
            else if (((int)Month < 8 && (int)Month % 2 == 0) || (int)Month > 7 && (int)Month % 2 != 0) // Month a 30Days
            {
                NbDays = 30;
            }
            else // Month a 31Days
            {
                NbDays = 31;
            }

            //Days de la semaine
            for (int j = 1; j <= NbDays; j++)
            {
                //Pour une date de la forme Day / Month / année où « Day » prend une valeur de 01 à 31, « Month » de 01 à 12 et « année » de 1583 à 9999, utiliser la formule:
                int c = (14 - (int)Month) / 12;
                // En fait, c = 1 pour janvier et février, c = 0 pour les autres Month.
                int a = Year.IntYear - c;
                int m = (int)Month + 12 * c - 2;

                int DaySemaine = (j + a + a / 4 - a / 100 + a / 400 + (31 * m) / 12) % 7;
                //La réponse obtenue pour j correspond alors à un Day de la semaine suivant :
                //0 = dimanche, 1 = lundi, 2 = mardi, etc. 
                //Dans toutes les divisions « / », on ne garde que la partie entière du résultat.

                Day newD = null;
                switch (DaySemaine)
                {
                    case 0:
                        newD = new Day(NameDays.DIMANCHE);
                        break;
                    case 1:
                        newD = new Day(NameDays.LUNDI);
                        break;
                    case 2:
                        newD = new Day(NameDays.MARDI);
                        break;
                    case 3:
                        newD = new Day(NameDays.MERCREDI);
                        break;
                    case 4:
                        newD = new Day(NameDays.JEUDI);
                        break;
                    case 5:
                        newD = new Day(NameDays.VENDREDI);
                        break;
                    case 6:
                        newD = new Day(NameDays.SAMEDI);
                        break;
                }
                if (newD != null)
                {

                    Days.Add(newD);
                }
            }
        }
    }
}
