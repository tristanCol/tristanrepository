﻿using DEBUG.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEBUG.Models.ColorModels
{
    public enum COLORS
    {
        RED = 1,
        GREEN = 2,
        YELLOW = 3
    }

    public class ColorObject : ISelectable
    {
        public int Id { get; set; }
        public COLORS Color { get; set; }

        public string ColorStr { get; set; }
        public string Word { get { return Color.ToString(); }  }

        public string Square { get; set; } = "■";
        public bool IsSelected { get; set; }

        public void ModifColor()// <-- Change de couleur en fct de celle déja présente
        {
            switch (Color)
            {
                case COLORS.RED:
                    Color = COLORS.GREEN;
                    break;
                case COLORS.GREEN:
                    Color = COLORS.YELLOW;
                    break;
                case COLORS.YELLOW:
                    Color = COLORS.RED;
                    break;
            }
        }
    }
}
