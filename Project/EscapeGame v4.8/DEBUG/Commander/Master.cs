﻿using DAL_Escape.Data;
using DAL_Escape.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DEBUG.Commander
{
    public class Master : Ascii
    {
        // LISTE DES ENIGMES POUR LA PARTIE
        public List<ICommander> Enigmes { get; set; }

        //PROP VERIFIANT SI LES ENIGMES SONT TOUTES RESOLUES
        private bool IsAllResolved {
            get
            {
                int cpt = 0;
                foreach (ICommander eni in Enigmes)
                {
                    if (eni.IsResolved)
                    {
                        cpt++;
                    }
                }
                return cpt == Enigmes.Count;
            }
        }

        // on affiche le menu avec les différentes énigmes (√ = résolue; ← = sélectionnée)
        public void ShowMenuEnigma()
        {
            Console.WriteLine();
            int nb = 25;
            for (int i = 0; i < Enigmes.Count; i++)
            {
                Console.SetCursorPosition(95, nb=nb+2);
                if (Enigmes[i].IsResolved)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                }
                else if (Enigmes[i].IsLocked)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                }
                Console.Write("  " + Enigmes[i].Name);
                if (Enigmes[i].IsResolved)
                    Console.Write(" √");
                else
                    Console.Write(" ");
                if (Enigmes[i].IsSelected)
                    Console.Write(" ←");
                else
                    Console.Write(" ");
                Console.ResetColor();
                Console.WriteLine();
            }
            Question();

            if (Enigmes[1].IsResolved)// Affiche l'indice pour l'enigme calendar
            {
                Console.SetCursorPosition(100, 45);
                Console.WriteLine($"{((Calendar)Enigmes[3]).WinDate.Day}:{((Calendar)Enigmes[3]).WinDate.Month}:{((Calendar)Enigmes[3]).WinDate.Year.ToString().Substring(2,2)}");
            }
        }

        public void LoadScreen()// Animation de "loading"
        {
            Console.Clear();
            Console.WriteLine();
            Console.SetCursorPosition(180, 45);
            Console.Write("LOA");
            Thread.Sleep(200);
            Console.Write("DING");
            Thread.Sleep(200);
            Console.Write("...");
            Thread.Sleep(200);
            Console.Clear();
        }

        // NAVIGATION DANS LE MENU
        public void NavInMenuEni()
        {
            Console.CursorVisible = false;

            ConsoleKey key = new ConsoleKey();
            int pos = 0;
            Enigmes[pos].IsSelected = true;

            //Boucle de jeu
            while (!IsAllResolved) {

                Thread th = new Thread(new ThreadStart(ThreadH)); //Thread pour l'horloge

                MainScreen();
                ShowMenuEnigma();

                th.Start();// Thread
                key = Console.ReadKey().Key;
                th.Abort();// Thread

                Console.Clear();
                if (key == ConsoleKey.H) {// H pour help
                    LoadScreen();
                    HelpScreen();
                    LoadScreen();
                }
                else if (key == ConsoleKey.E)// E pour sélectionner l'enigme
                { 
                    if (!Enigmes[pos].IsLocked) {
                        if (!Enigmes[pos].IsResolved)
                        {
                            LoadScreen();
                            Enigmes[pos].Jouer();
                            if (Enigmes[pos].IsResolved)
                                RabbitScreen();
                            LoadScreen();
                        }
                    }
                }
                else
                    pos = SeDeplacer(key, pos);

                if (Enigmes[2].IsResolved)
                {
                    Enigmes[3].IsLocked = false;
                }
            }

            // on switch de couleur en clignotant pour la fin du jeu
            bool colorSwitch = true;
            int nb = 0;
            while (nb < 30)
            {
                if (colorSwitch)
                    Console.ForegroundColor = ConsoleColor.Yellow;
                else
                    Console.ForegroundColor = ConsoleColor.Red;
                WinScreen();
                Thread.Sleep(500);
                Console.Clear();
                colorSwitch = !colorSwitch;
                Thread.Sleep(500);
                nb++;
            }
        }

        // methode pour se déplacer
        int SeDeplacer(ConsoleKey keyD, int posD)
        {
            Enigmes[posD].IsSelected = false;
            switch (keyD)
            {
                case ConsoleKey.UpArrow:
                    if (posD > 0)
                    {
                        posD--;
                    }
                    break;
                case ConsoleKey.DownArrow:
                    if (posD < Enigmes.Count- 1)
                    {
                        posD++;
                    }
                    break;
            }
            Enigmes[posD].IsSelected = true;
            return posD;
        }

        public void RabbitScreen()
        {
            Console.Clear();
            WinScreenEnigma();
            Thread.Sleep(2500);
        }

        public void ThreadH()
        {
            
            while (!Enigmes[1].IsResolved)
            {
                Console.SetCursorPosition(100, 45);
                Console.Write(DateTime.Now.ToLongTimeString());
                Thread.Sleep(10);
                Console.Write("");
            }
        }
    }
}
