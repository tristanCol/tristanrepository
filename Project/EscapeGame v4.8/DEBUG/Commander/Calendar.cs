﻿using DAL_Escape.Data.CalendarEnigma;
using DEBUG.Models.CalendarModels.Calendar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DEBUG.Commander
{
    public class Calendar : Ascii, ICommander
    {
        public string Name { get; set; }// <-- Nom de l'énigme

        public List<Year> Years { get; set; }// <-- Liste des années du calendrier

        // TRANCHE DES ANNEES
        public int FIRSTYEAR { get; }
        public int LASTYEAR { get; }

        public CalendarSolution WinDate { get; set; }// <-- Date où entrer l'event pour gagner

        //Bool props
        public bool IsPlaying { get; set; }
        public bool IsResolved { get; set; }
        public bool IsLocked { get; set; }
        public bool IsSelected { get; set; }

        // CTOR
        public Calendar(CalendarSolution winD)
        {
            Years = new List<Year>();
            Year ann = null;
            for (int i = 1583; i < 2120; i++) // 1583 = FIRSTYEAR (algorithme calculant les jours fonctionne à partir de 1583) 2120 = LASTYEAR
            {
                ann = new Year(i);
                Years.Add(ann);
            }

            //utilisation du linq avec lambda
            FIRSTYEAR = Years.Select(a => a.IntYear).First<int>();
            LASTYEAR = Years.Select(a => a.IntYear).Last<int>();

            WinDate = winD;

            IsPlaying = true;
            IsResolved = false;
            IsLocked = true;
            IsSelected = false;

            Name = "Calendar.enigma";

            // EVENT EASTEREGG
            Years[1995 - FIRSTYEAR].Months[10].Days[19 - 1].Event = "ANNIVERSAIRE DU TYPE BIZARRE";
            Years[1997 - FIRSTYEAR].Months[2].Days[3 - 1].Event = "ANNIVERSAIRE DU TYPE GENIAL";
            Years[2019 - FIRSTYEAR].Months[4].Days[4 - 1].Event = "DEBUT DE LA FORMATION";
            Years[2019 - FIRSTYEAR].Months[11].Days[12 - 1].Event = "PRESENTATION DU PROJET";
            Years[2019 - FIRSTYEAR].Months[12].Days[25 - 1].Event = "Joyeux Noël";
        }

        public void Afficher(int month, int year)
        {
            // PLACEMENT SUR L'ECRAN
            int top = 21;
            int left = 81;

            for (int i = 0; i < Years.Count; i++)
            {
                if (Years[i].IntYear == year)// ANNEE A AFFICHER
                {
                    for (int j = 1; j <= 12; j++)
                    {
                        if (Years[i].Months[j].IntMonth == month)// MOIS A AFFICHER
                        {
                            Console.SetCursorPosition(left, top += 1);// Place au bon endroit puis incremente pour element suivant

                            Console.WriteLine($"              {year} - {(Years[i].Months[j].Name)}");// ANNEE - MOIS Au dessus du calendrier

                            Console.SetCursorPosition(left, top += 2);// Place au bon endroit puis incremente pour element suivant
                            for (int k = 0; k < 7; k++)
                            {
                                string jSem = Years[i].Months[j].Days[k].Name.ToString().Substring(0, 2);// Affiche les jours de la semaine
                                Console.Write($"  {jSem}  ");
                            }
                            Console.WriteLine();

                            Console.SetCursorPosition(left, top+=1);// Place au bon endroit puis incremente pour element suivant
                            for (int k = 0; k < 7; k++)
                                Console.Write("------");// Haut du calendrier
    
                            int cpt = 0;// cmpt des jours du mois

                            foreach (Day jrs in (Years[i].Months[j].Days))
                            {
                                if (jrs.IsSelected)
                                {
                                    Console.ForegroundColor = ConsoleColor.Green;
                                }
                                else if (jrs.Event != "")// Mets en bleu les jours ayant un évenement
                                {
                                    Console.ForegroundColor = ConsoleColor.Blue;
                                }
                                if (cpt % 7 == 0)// passe a ligne après 7 jours affiché
                                {
                                    Console.WriteLine();
                                    Console.SetCursorPosition(left, top+=1);
                                }

                                Console.Write($"| ");// Affiche les jours
                                if (cpt < 9)
                                    Console.Write("0");
                                Console.Write($"{ cpt + 1} |");
                                Console.ResetColor();
                                cpt++;
                            }

                            for (int n = 0; n < 35 - cpt; n++)// Complete le tableau d'espace vide pour former un rectangle (35 => 5x7=35 ET 31 jours maximum)
                            {
                                if (n != 35 - cpt - 1)
                                    Console.Write("      ");
                                else
                                    Console.Write("     |");
                            }
                            Console.WriteLine();

                            Console.SetCursorPosition(left, top+=1);
                            for (int k = 0; k < 7; k++)
                                Console.Write("------");// Bas du calendrier
                        }
                    }
                }
            }
            Console.SetCursorPosition(0, 32);
            Footer();
        }

        public void SeDeplacer(ref int year, ref int month, ref int day, ConsoleKey key)
        {
            switch (key)
            {
                case ConsoleKey.Z:// ↑ années
                    if (year < LASTYEAR)
                    {
                        year++;
                        day = 0;
                    }
                    else
                    {
                        year = LASTYEAR;
                    }
                    break;
                case ConsoleKey.S:// ↓ années
                    if (year > FIRSTYEAR)
                    {
                        year--;
                        day = 0;
                    }
                    else
                    {
                        year = FIRSTYEAR;
                    }
                    break;
                case ConsoleKey.Q://  ↓ mois
                    if (month > 1)
                    {
                        month--;
                    }
                    else
                    {
                        month = 12;
                        if (year > FIRSTYEAR)
                        {
                            year--;
                        }
                    }
                    day = 0;
                    break;
                case ConsoleKey.D:// ↑ mois
                    if (month < 12)
                    {
                        month++;
                    }
                    else
                    {
                        month = 1;
                        if (year < LASTYEAR)
                        {
                            year++;
                        }
                    }
                    day = 0;
                    break;
                case ConsoleKey.LeftArrow:// Déplacement gauche 
                    if (day > 0)
                    {
                        day--;
                    }
                    else
                    {
                        day = Years[year - FIRSTYEAR].Months[month].NbDays - 1;
                    }
                    break;
                case ConsoleKey.RightArrow:// Déplacement droite
                    if (day < Years[year - FIRSTYEAR].Months[month].NbDays - 1)
                    {
                        day++;
                    }
                    else
                    {
                        day = 0;
                    }
                    break;
                case ConsoleKey.UpArrow:// Déplacement haut
                    if (day >= 7)
                        day -= 7;
                    break;
                case ConsoleKey.DownArrow:// Déplacement bas
                    if (day < Years[year - FIRSTYEAR].Months[month].NbDays - 7)
                        day += 7;
                    break;

                case ConsoleKey.E:// Ajout d'un evenement a la date selectionee
                    AddEventScreen();
                    Console.SetCursorPosition(82, 12);
                    Console.WriteLine($"Event du {day + 1}/{month}/{year}: {Years[year - FIRSTYEAR].Months[month].Days[day].Event}");
                    Console.SetCursorPosition(84, 15);
                    Console.WriteLine("Press E to add event");
                    key = Console.ReadKey().Key;
                    if (key == ConsoleKey.E)
                    {
                        AddEventScreen();
                        AjouterEvent(year, month, day);
                    }
                    break;
                
            }
        }

        public void AjouterEvent(int year, int month, int day)
        {
            Console.SetCursorPosition(87, 12);
            Console.WriteLine("EVENT A AJOUTER:");
            Console.SetCursorPosition(104, 12);
            string evt = Console.ReadLine();
            Years[year - FIRSTYEAR].Months[month].Days[day].Event = evt;
        }

        public void Jouer()
        {
            CalendarScreen();// <-- Affichage décor
            EnigmaScreen();

            ConsoleKey key = new ConsoleKey();
            key = ConsoleKey.P;

            int year = DateTime.Now.Year;// <-- Date d'ajd comme premier jour sélectionné
            int month = DateTime.Now.Month;
            int day = DateTime.Now.Day - 1;


            while (IsPlaying)
            {
                Years[year - FIRSTYEAR].Months[month].Days[day].IsSelected = true;// <-- Affichage avec le jour sélectionné
                Afficher(month, year);
                Years[year - FIRSTYEAR].Months[month].Days[day].IsSelected = false;

                key = Console.ReadKey().Key;
                if (key == ConsoleKey.Escape)// <-- Escape pour quitter l'enigme en cours
                    return;

                SeDeplacer(ref year, ref month, ref day, key);// <-- Déplacement dans le calendrier

                Console.Clear();
                CalendarScreen();// <-- Affichage décor
                EnigmaScreen();


                IsPlaying = VerifWin(year, month, day);
            }
        }
        

        public bool VerifWin(int year, int month, int day)
        {
            if (Years[WinDate.Year - FIRSTYEAR].Months[WinDate.Month].Days[WinDate.Day - 1].Event == WinDate.Event)
            {
                IsResolved = true;
                Console.ResetColor();
                return false;
            }
            return true;
        }
    }
}
