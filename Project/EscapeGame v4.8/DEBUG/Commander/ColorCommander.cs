﻿using DEBUG.Mapper;
using DEBUG.Models.ColorModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DEBUG.Commander
{
    public class ColorCommander : Ascii, ICommander
    {
        public string Name { get; set; }// <-- Nom de l'énigme

        public List<ColorObject> ColorObjects { get; set; }// <-- Liste des objets de l'enigme
        public string Solution { get; set; }// <-- Concaténation des "Word" des "BookObject" dans le bon ordre

        //Bool props:
        public bool IsPlaying { get; set; }
        public bool IsResolved { get ; set ; }
        public bool IsLocked { get ; set; }
        public bool IsSelected { get ; set; }

        public List<List<string>> IconList{ get; set; }// <-- Liste des images pour l'affichage

        // CTOR
        public ColorCommander(List<ColorObject> colorObjects, string solution)
        {
            IconList = new List<List<string>>();
            foreach (ColorObject color in colorObjects) // Liste des images (Gros carrés)
            {
                IconList.Add(GetSquareIcon());
            }

            foreach (ColorObject item in colorObjects)  // Liste des boutons (Petits carrés)
            {
                switch (item.ColorStr)
                {
                    case "RED":
                        item.Color = COLORS.RED;
                        break;
                    case "YELLOW":
                        item.Color = COLORS.YELLOW;
                        break;
                    case "GREEN":
                        item.Color = COLORS.GREEN;
                        break;
                    default:
                        break;
                }
            }

            ColorObjects = colorObjects;
            Solution = solution;

            IsPlaying = true;
            IsResolved = false;
            IsLocked = false;
            IsSelected = false;

            Name = "Color.enigma";
        }

        public void Afficher()
        {
            // AFFICHAGE DES BOUTONS
            Console.SetCursorPosition(95, 20);
            foreach (ColorObject item in ColorObjects)
            {
                Console.Write(" ");
                switch (item.Color)
                {
                    case COLORS.YELLOW:
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.Write(item.Square);
                        Console.ResetColor();
                        break;
                    case COLORS.GREEN:
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write(item.Square);
                        Console.ResetColor();
                        break;
                    case COLORS.RED:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(item.Square);
                        Console.ResetColor();
                        break;
                }

                if (item.IsSelected)
                {
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.Write("← ");
                    Console.ResetColor();
                }
                else
                {
                    Console.Write("  ");
                }
            }
            Console.WriteLine();

            // AFFICHAGE DES IMAGES
            int left = 61;
            int top = 25;

            for (int i = 0; i < IconList[1].Count; i++)// <--- i représente la hauteur
            {
                Console.SetCursorPosition(left, top++);
                for (int j = 0; j < IconList.Count; j++)// <--- j représente l'objet 
                {
                    switch (ColorObjects[j].Color)// <--- Selectionne la couleur
                    {
                        case COLORS.YELLOW:
                            Console.ForegroundColor = ConsoleColor.Green;
                            break;
                        case COLORS.GREEN:
                            Console.ForegroundColor = ConsoleColor.Red;
                            break;
                        case COLORS.RED:
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            break;
                    }
                    Console.Write(IconList[j][i] + "     ");
                    Console.ResetColor();
                }
                Console.WriteLine();
            }

            Console.SetCursorPosition(0, 32);
            Footer();
        }

        public int SeDeplacer(ConsoleKey key, int pos)
        {
            ColorObjects[pos].IsSelected = false;

            pos = key.MoveLeftOrRigth(pos, ColorObjects.Count);

            ColorObjects[pos].IsSelected = true;
            return pos;
        }

        public void Jouer()
        {
            ColorScreen();
            EnigmaScreen();
            int pos = 0;
            

            ConsoleKey key = new ConsoleKey();
            while (IsPlaying)
            {
                Afficher();
                key = Console.ReadKey().Key;
                ColorScreen();
                EnigmaScreen();
                if (key == ConsoleKey.E)// E pour changer la couleur
                {
                    ColorObjects[pos].ModifColor();
                }
                else if (key == ConsoleKey.Escape)// Escape pour quitter l'enigme en cours
                    return;
                else
                {
                    pos = SeDeplacer(key, pos);
                }

                IsResolved = !ColorObjects.VerifWin(Solution);
                IsPlaying = !IsResolved;
            }
        }
    }
}