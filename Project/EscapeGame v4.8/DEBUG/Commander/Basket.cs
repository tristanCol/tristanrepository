﻿
using DEBUG.Commander;
using DEBUG.Interfaces;
using DEBUG.Mapper;
using DEBUG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DEBUG
{
    // COMMANDER DE L'ENIGME BASKET
    public class Basket : Ascii, ICommander
    {
        public string Name { get; set; }

        public List<BasketObject> BasketObjects { get; set; }// <-- Liste des objets de l'enigme
        public string Solution { get; set; }// <-- Concaténation des "Word" des "BasketObject" dans le bon ordre

        //Bool props:
        public bool IsPlaying { get; set; }
        public bool IsResolved { get ; set ; }
        public bool IsLocked { get ; set ; }
        public bool IsSelected { get; set ; }

        public List<List<string>> IconList { get; set; }// <-- Liste des images pour l'affichage

        // CTOR
        public Basket(List<BasketObject> listBaO, string solution)
        {
            BasketObjects = listBaO.ShuffleWithoutWin(solution);

            // Création de la liste des images pour l'affichage
            IconList = new List<List<string>>();
            foreach (BasketObject bo in BasketObjects)
            {
                IconList.Add(GetExtIcon(bo.Extention));
            }

            Solution = solution;
            IsPlaying = true;
            IsResolved = false;
            IsLocked = false;
            IsSelected = false;

            Name = "Basket.enigma";
        }

        // METHODE DE L'AFFICHAGE
        public void Afficher()
        {
            BasketObjects.AffichageWithImg(4, 24, IconList, 11);
        }

        public int SeDeplacer(ConsoleKey key, int pos)//<-- renvoi la nouvelle position
        {
            BasketObjects[pos].IsSelected = false;

            pos = key.MoveLeftOrRigth(pos, BasketObjects.Count);// <-- Déplacement gauche/droite

            BasketObjects[pos].IsSelected = true;//<-- Sélectionne l'objet suivant/précédent
            return pos;
        }

        public int DeplacerObj(int posFile) //<-- renvoi la position de l'objet à déplacer
        {
            BasketObject f = BasketObjects[posFile]; // retiens le fichier à deplacer
            List<string> l = IconList[posFile]; // retiens l'image à deplacer

            return f.MoveObject(posFile, BasketObjects, IconList, 11, 4, 24);
        }


        // METHODE UTILISEE PAR LE MASTER COMMANDER
        public void Jouer()
        {
            BasketScreen();// <-- Affichage décor
            EnigmaScreen();// <-- Affichage décor

            int pos = 0;// <-- Position de départ
            BasketObjects[pos].IsSelected = true;
            while (IsPlaying)
            {
                Afficher();

                ConsoleKey key = Console.ReadKey().Key;// <-- Input

                BasketScreen();
                EnigmaScreen();

                if (key == ConsoleKey.E)// <-- e pour déplacer le fichier
                {
                    pos = DeplacerObj(pos);
                }
                else if (key == ConsoleKey.Escape)// <-- Escape pour quitter l'énigme en cours
                {
                    BasketObjects[pos].IsSelected = false;
                    Console.ResetColor();
                    return;
                }
                else// <-- Flèches pour se déplacer
                {
                    pos = SeDeplacer(key, pos);
                }

                IsPlaying = BasketObjects.VerifWin(Solution);
                IsResolved = !IsPlaying;
            }
        }
    }
}