﻿
using DEBUG.Interfaces;
using DEBUG.Mapper;
using DEBUG.Models.BookModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DEBUG.Commander
{
    
    public class Library : Ascii, ICommander
    {
        public string Name { get; set; }// <-- Nom de l'énigme

        public List<BookObject> BookObjects { get; set; }// <-- Liste des objets de l'enigme
        public string Solution { get; set; }// <-- Concaténation des "Word" des "BookObject" dans le bon ordre

        //Bool props:
        public bool IsPlaying { get; set; }
        public bool IsResolved { get; set; }
        public bool IsLocked { get; set; }
        public bool IsSelected { get; set; }

        public List<List<string>> IconList { get; set; }// <-- Liste des images pour l'affichage

        // CTOR
        public Library(List<BookObject> listBoO, string solution)
        {
            BookObjects = listBoO.ShuffleWithoutWin(solution);

            // Création de la liste des images pour l'affichage
            IconList = new List<List<string>>();
            foreach (BookObject bo in BookObjects)
            {
                IconList.Add(GetBookIcon());
            }

            Solution = solution;
            IsPlaying = true;
            IsResolved = false;
            IsLocked = false;
            IsSelected = false;

            Name = "Library.enigma";
        }

        public void Afficher()
        {
            BookObjects.AffichageWithImg(30, 20, IconList, 37);
        }

        public int SeDeplacer(ConsoleKey key, int pos) //renvoi la position
        {
            BookObjects[pos].IsSelected = false;

            pos = key.MoveLeftOrRigth(pos, BookObjects.Count);// <-- Déplacement gauche/droite

            BookObjects[pos].IsSelected = true;// <-- Sélectionne l'objet suivant/précédent
            return pos;
        }

        public int DeplacerObj(int posFile) // renvoi la position
        {
            BookObject f = BookObjects[posFile]; // retiens le fichier a deplacer
            List<string> l = IconList[posFile];

            return f.MoveObject(posFile, BookObjects, IconList, 37, 30, 20);
        }

        // METHODE UTILISEE PAR LE MASTER COMMANDER
        public void Jouer()
        {
            LibraryScreen();// <-- Affichage décor
            EnigmaScreen();// <-- Affichage décor

            int pos = 0;// <-- position de départ
            BookObjects[pos].IsSelected = true;
            while (IsPlaying)
            {
                Afficher();

                ConsoleKey key = Console.ReadKey().Key;// <-- Input

                LibraryScreen();
                EnigmaScreen();
                
                if (key == ConsoleKey.E)// <-- e pour deplacer le fichier
                {
                    pos = DeplacerObj(pos);
                }
                else if (key == ConsoleKey.Escape)// <-- escape pour quitter l'énigme en cours
                {
                    BookObjects[pos].IsSelected = false;
                    Console.ResetColor();
                    return;
                }
                else// <-- fleches pour se deplacer
                {
                    pos = SeDeplacer(key, pos);
                }

                IsResolved = !BookObjects.VerifWin(Solution);
                IsPlaying = !IsResolved;
            }
        }
    }
}