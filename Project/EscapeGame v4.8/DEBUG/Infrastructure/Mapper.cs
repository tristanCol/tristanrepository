﻿using DLBa = DAL_Escape.Data;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGBa = DEBUG.Models;
using DLBo = DAL_Escape.Data.BookEnigma;
using DGBo = DEBUG.Models.BookModels;
using DLC = DAL_Escape.Data.ColorEnigma;
using DGC = DEBUG.Models.ColorModels;

namespace DEBUG.Mapper
{
    // MAPPER POUR LA CONVERSION DE LA DAL AU PROGRAMME
    public static class Mapper
    {
        public static DGBa.BasketObject ToBasketObjectDEBUG(this DLBa.BasketObject entity ) => new DGBa.BasketObject
        {
            Id = entity.IdEnigma,
            Word = entity.Word,
            IsSelected = false
        };
        
        public static DGBo.BookObject ToBookObjectDEBUG(this DLBo.BookObject entity) => new DGBo.BookObject
        {
            Id = entity.IdEnigma,
            Auteur = entity.Auteur,
            Word = entity.Titre,
            NbPages = entity.NbPages,
            IsSelected = false
        };

        public static DGC.ColorObject ToColorObjectDEBUG(this DLC.ColorObject entity) => new DGC.ColorObject
        {
            Id = entity.IdEnigma,
            ColorStr = entity.Color,
            IsSelected = false
        };
    }
}