﻿using DAL_Escape.Data;
using DEBUG.Commander;
using DEBUG.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEBUG.Mapper
{
    public static class EnigmaFunc
    {
        // MELANGE DE LISTE
        public static void Shuffle<T>(this IList<T> list)
        {
            Random rnd = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rnd.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        // MELANGE LA LISTE D'OBJETS ET EMPECHE L'INSTANT WIN
        public static List<T> ShuffleWithoutWin<T>(this List<T> list, string solution)
            where T: ISelectable
        {
            string currentStr = "";
            do
            {
                list.Shuffle();// <-- Mélange la liste des objets
                foreach (T item in list)
                {
                    currentStr += item.Word;

                }
            } while (currentStr == solution);// <-- Vérifie que la liste n'est pas égale à la solution
            return list;
        }

        // METHODE POUR CENTRER SOUS LES IMAGES
        public static string CenterUnderImg(this string word, int imgWidth)
        {
            int nbSpaces = imgWidth - word.Length;// <-- Nombre d'espaces nécessaire

            if (word.Length < imgWidth)
            {
                for (int p = 0; p < nbSpaces / 2; p++)// <-- Ajoute les espaces avant le "word"
                {
                    word = " " + word;

                }
                while (word.Length < imgWidth)// <-- Ajoute les espaces après le "word"
                {
                    word += " ";
                }
            }

            return word;
        }

        // METHODE AFFICHAGE
        public static void AffichageWithImg<T>(this List<T> objects, int left, int top, List<List<string>> imgs, int imgWidth)
            where T: ISelectable
        {
            Ascii ascii = new Ascii();
            // AFFICHAGE DES ICONES
            for (int i = 0; i < imgs[1].Count; i++)  // Affiche les images ligne par ligne
            {                                            // Les images ne sont donc pas générée les unes après les autres mais
                Console.SetCursorPosition(left, top++);  // toutes en même temps, du haut vers le bas
                for (int j = 0; j < imgs.Count; j++)
                {
                    if (objects[j].IsSelected)// <-- L'objet sélectionné est affiché en vert
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                    }
                    Console.Write(imgs[j][i] + "        ");// <-- Espace entre chaque image
                    Console.ResetColor();
                }
                Console.WriteLine();
            }

            // AFFICHAGE WORD
            Console.SetCursorPosition(left, top);
            foreach (T obj in objects)
            {
                if (obj.IsSelected)// <-- L'objet sélectionné est affiché en vert
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                }

                // CENTRE LES MOTS SOUS LES IMAGES
                string wordWithSpace = obj.Word.CenterUnderImg(imgWidth);
                Console.Write(wordWithSpace);
                Console.ResetColor();
                Console.Write("        ");
            }

            Console.SetCursorPosition(0, 32);// <-- Affichage du footer
            ascii.Footer();
        }

        // METHODE DE DEPLACEMENT ← →
        public static int MoveLeftOrRigth(this ConsoleKey key, int pos, int listCount)
        {
            switch (key)//<-- fleches gauche/droite
            {
                case ConsoleKey.RightArrow:
                    if (pos < listCount - 1)
                    {
                        pos++;
                    }
                    break;
                case ConsoleKey.LeftArrow:
                    if (pos > 0)
                    {
                        pos--;
                    }
                    break;
            }
            return pos;
        }

        // METHODE DE DEPLACEMENT D'OBJET
        public static int MoveObject<T>(this T obj, int posFile, List<T> list, List<List<string>> imgs, int imgWidth, int left, int top)
            where T : ISelectable//<-- renvoi la position de l'objet à déplacer
        {
            Ascii ascii = new Ascii();// <-- Affichage décor

            T f = list[posFile]; // retiens le fichier à deplacer

            ConsoleKey key = new ConsoleKey();
            while (key != ConsoleKey.E) // e ==> place le fichier à l'endroit désiré
            {
                int pos = posFile; // retiens la position du fichier à deplacer
                list.AffichageWithImg(left, top, imgs, imgWidth);

                key = Console.ReadKey().Key;

                ascii.ColorsAreNot();

                posFile = key.MoveLeftOrRigth(posFile, list.Count); // utilise la fct se deplacer
                list[pos] = list[posFile]; // met le fichier a qui ont prends sa place dans celle de notre fichier retenu plus haut

                list[pos].IsSelected = false;

                list[posFile] = f; // met notre fichier dans sa nouvelle pos

                list[posFile].IsSelected = true;
            }

            list[posFile].IsSelected = false;
            return posFile;
        }

        // METHODE VERIFICATION DE VICTOIRE (Pour la concaténation de string)
        public static bool VerifWin<T>(this List<T> list, string solution)
            where T: ISelectable
        {
            string currentStr = "";
            foreach (T item in list)// <-- Concatène les "Word"
            {
                currentStr += item.Word;
            }
            if (currentStr.ToLower() == solution.ToLower())// <-- Vérifie la concaténation pour la victoire
            {
                Console.ResetColor();
                return false; // <-- return false car affecte "IsPlaying" <= Victoire
            }
            return true;// <-- Enigme non résolue
        }

        public static void GetBirthDate(Calendar calendar)
        {
            int day;
            int month;
            int year;
            string name;
            string input;

            ConsoleKey verif = new ConsoleKey();
            do
            {
                day = 0;
                month = 0;
                year = 1582;
                name = "";

                int baseleft = 84;
                int basetop = 24;

                Console.SetCursorPosition(0, basetop+2);
                Console.Write(new string(' ', Console.WindowWidth));

                do
                {
                    Console.SetCursorPosition(baseleft, basetop);
                    Console.Write("Birth Day:");
                    Console.SetCursorPosition(95, basetop);
                    Console.Write(new string(' ', Console.WindowWidth));
                    Console.SetCursorPosition(95, basetop);
                    input = Console.ReadLine();
                    try
                    {
                        day = int.Parse(input);
                    }
                    catch (Exception)
                    {
                        Console.SetCursorPosition(baseleft, basetop + 2);
                        Console.Write("Invalid day");
                    }

                } while (day < 1 || day > 31);

                Console.SetCursorPosition(baseleft, basetop + 2);
                Console.Write(new string(' ', Console.WindowWidth));

                do
                {
                    Console.SetCursorPosition(baseleft, basetop);
                    Console.WriteLine("Birth Month:");
                    Console.SetCursorPosition(96, basetop);
                    Console.Write(new string(' ', Console.WindowWidth));
                    Console.SetCursorPosition(97, basetop);
                    input = Console.ReadLine();
                    try
                    {
                        month = int.Parse(input);
                    }
                    catch (Exception)
                    {
                        Console.SetCursorPosition(baseleft, basetop + 2);
                        Console.WriteLine("Invalid month");
                    }

                } while (month < 1 || month > 12);

                Console.SetCursorPosition(baseleft, basetop + 2);
                Console.Write(new string(' ', Console.WindowWidth));
                do
                {
                    Console.SetCursorPosition(baseleft, basetop);
                    Console.WriteLine("Birth Year:");

                    Console.SetCursorPosition(95, basetop);
                    Console.WriteLine(" ");
                    Console.SetCursorPosition(96, basetop);
                    Console.Write(new string(' ', Console.WindowWidth));
                    Console.SetCursorPosition(96, basetop);
                    input = Console.ReadLine();
                    try
                    {
                        year = int.Parse(input);
                    }
                    catch (Exception)
                    {
                        Console.SetCursorPosition(baseleft, basetop + 2);
                        Console.WriteLine("Invalid year");
                    }

                } while (year < 1583 || year > DateTime.Now.Year);

                Console.SetCursorPosition(baseleft, basetop + 2);
                Console.Write(new string(' ', Console.WindowWidth));
                Console.SetCursorPosition(baseleft, basetop);
                Console.WriteLine("Enter your Nickname:");
                Console.SetCursorPosition(105, basetop);
                name = Console.ReadLine();
                Console.SetCursorPosition(baseleft-14, basetop + 2);
                Console.WriteLine($"Are you {name}, born the {day}/{month}/{year}? (press Y to confirm)");
                verif = Console.ReadKey().Key;
            } while (verif != ConsoleKey.Y);


            calendar.Years[year - calendar.FIRSTYEAR].Months[month].Days[day-1].Event = $"{name}'s birthday... Beware of the clock!";
        }
    }
}
