﻿using DEBUG.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEBUG
{
    public interface ICommander
    {
        string Name { get; set; }// <-- Nom de l'énigme

        bool IsPlaying { get; set; }// <-- L'énigme est-elle en cours de jeu ou non
        bool IsResolved { get; set; }// <-- L'énigme est-elle résolue
        bool IsLocked { get; set; }// <-- L'énigme est-elle jouable ou non
        bool IsSelected { get; set; }// <-- L'énigme est-elle selectionnée (pour la sélection dans le menu)


        void Jouer();
    }
}
