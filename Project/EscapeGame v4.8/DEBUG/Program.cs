﻿
using DLBa = DAL_Escape.Data;
using DAL_Escape.Services;
using DGBa = DEBUG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DLBo = DAL_Escape.Services.BookServices;
using DGBo = DEBUG.Models.BookModels;
using DAL_Escape.Data;
using DAL_Escape.Data.BookEnigma;
using DAL_Escape.Services.BookServices;
using DEBUG.Commander;
using DAL_Escape.Services.ColorServices;
using DLC = DAL_Escape.Data.ColorEnigma;
using DGC = DEBUG.Models.ColorModels;
using DAL_Escape.Data.CalendarEnigma;
using DAL_Escape.Services.CalendarServices;
using System.Runtime.InteropServices;
using DEBUG.Mapper;

namespace DEBUG
{
    class Program
    {
        [DllImport("kernel32.dll", ExactSpelling = true)]

        private static extern IntPtr GetConsoleWindow();
        private static IntPtr ThisConsole = GetConsoleWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]

        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow); // <-- Permet de modifier l'affichage de la fenêtre
        private const int HIDE = 0;
        private const int MAXIMIZE = 3;
        private const int MINIMIZE = 6;
        private const int RESTORE = 9;

        static void Main(string[] args)
        {
            Console.SetWindowSize(Console.LargestWindowWidth, Console.LargestWindowHeight);// <-- Fullscreen
            ShowWindow(ThisConsole, MAXIMIZE);

            // avoir l'unicode en console
            Console.OutputEncoding = Encoding.Unicode;

            #region Basket
            BasketObjectService boService = new BasketObjectService();

            List<DGBa.BasketObject> BasketObjects = (boService.GetByEnigma(1).Select(s => s.ToBasketObjectDEBUG()).ToList());
            BasketSolutionService boSolService = new BasketSolutionService();
            DLBa.BasketSolution bSol = boSolService.Get().Where(w => w.Id == 1).SingleOrDefault();

            Basket basket = new Basket(BasketObjects, bSol.Solution);
            #endregion

            #region Library
            DLBo.BookObjectService boOService = new DLBo.BookObjectService();
            List<DGBo.BookObject> bookObjects = (boOService.Get().Where(w => w.IdEnigma == 2).Select(s => s.ToBookObjectDEBUG()).ToList());
            BookSolutionService bookSolutionService = new BookSolutionService();

            BookSolution bookSolution = bookSolutionService.Get().Where(w => w.IdEnigma == 2).SingleOrDefault();
            Library library = new Library(bookObjects, bookSolution.Solution);
            #endregion

            #region Color
            ColorObjectService colorObService = new ColorObjectService();
            List<DGC.ColorObject> colorObjects = colorObService.Get().Where(w => w.IdEnigma == 3).Select(s => s.ToColorObjectDEBUG()).ToList();
            ColorSolutionService colorSolutionService = new ColorSolutionService();
            DLC.ColorSolution colSolution = colorSolutionService.Get().Where(w => w.IdEnigma == 3).SingleOrDefault();

            ColorCommander colorCommander = new ColorCommander(colorObjects, colSolution.Solution);
            #endregion

            #region Calendar
            CalendarSolutionService calendarService = new CalendarSolutionService();
            CalendarSolution calendarSolution = calendarService.Get().Where(w => w.IdEnigma == 4).SingleOrDefault();
            Calendar Calendar = new Calendar(calendarSolution);
            #endregion

            Master master = new Master()
            {
                Enigmes = new List<ICommander>()
                {
                    basket,
                    library,
                    colorCommander,
                    Calendar
                }
            };



            master.WelcomeScreen();
            EnigmaFunc.GetBirthDate(Calendar);
            master.LoadScreen();
            master.NavInMenuEni();

            #region Add Enigma
            //Ajout d'enigme:

            //Enigma Enigma = new Enigma()
            //{Name = "Calendar1"
            //};
            //EnigmaService eService = new EnigmaService();

            //eService.Insert(Enigma);
            //Console.ReadLine();

            #endregion
        }
    }
}
