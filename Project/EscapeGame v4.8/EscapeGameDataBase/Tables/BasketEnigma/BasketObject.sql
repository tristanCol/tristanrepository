﻿CREATE TABLE [dbo].[BasketObject]
(
	[IdBasketObject] INT IDENTITY, 
    [IdEnigma] INT NOT NULL,
	[Word] NVARCHAR(26) NOT NULL,

	CONSTRAINT PK_IdBasketObject PRIMARY KEY(IdBasketObject),
	CONSTRAINT FK_IdEnigmaBasketObject FOREIGN KEY (IdEnigma) REFERENCES Enigma(IdEnigma),
)
