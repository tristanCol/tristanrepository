﻿CREATE TABLE [dbo].[BasketSolution]
(
	[IdBasketSolution] INT NOT NULL IDENTITY,
	[IdEnigma] INT NOT NULL,
    [Solution] NVARCHAR(250) NOT NULL,
	
	CONSTRAINT PK_IdBasketSolution PRIMARY KEY(IdBasketSolution),
	CONSTRAINT FK_IdEnigmaBasketSolution FOREIGN KEY(IdEnigma) REFERENCES Enigma(IdEnigma)
)
