﻿CREATE TABLE [dbo].[CalendarSolution]
(
	[IdCalendarSolution] INT IDENTITY, 
    [IdEnigma] INT NOT NULL,
	[Day] INT NOT NULL,
	[Month] INT NOT NULL,
	[Year] INT NOT NULL,
	[Event] NVARCHAR(50)

	CONSTRAINT PK_IdCalendarSolution PRIMARY KEY(IdCalendarSolution) NOT NULL,
	CONSTRAINT FK_IdEnigmaCalendarSolution FOREIGN KEY (IdEnigma) REFERENCES Enigma(IdEnigma),
)
