﻿CREATE TABLE [dbo].[BookSolution]
(
	[IdBookSolution] INT NOT NULL IDENTITY,
	[IdEnigma] INT NOT NULL,
    [Solution] NVARCHAR(MAX) NOT NULL,
	
	CONSTRAINT PK_IdBookSolution PRIMARY KEY(IdBookSolution),
	CONSTRAINT FK_IdEnigmaBookSolution FOREIGN KEY(IdEnigma) REFERENCES Enigma(IdEnigma)
)
