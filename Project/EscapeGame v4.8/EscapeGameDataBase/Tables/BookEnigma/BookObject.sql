﻿CREATE TABLE [dbo].[BookObject]
(
	[IdBookObject] INT NOT NULL IDENTITY, 
    [IdEnigma] INT NOT NULL,
	

	[Titre] NVARCHAR(125) NULL, 
    [Auteur] NVARCHAR(75) NULL, 
    [NbPages] INT NULL, 
    CONSTRAINT PK_BookObject PRIMARY KEY(IdBookObject),
	CONSTRAINT FK_IdEnigmaBookObject FOREIGN KEY(IdEnigma) REFERENCES Enigma(IdEnigma)
)
