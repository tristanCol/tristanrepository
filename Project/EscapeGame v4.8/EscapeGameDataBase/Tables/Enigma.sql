﻿CREATE TABLE [dbo].[Enigma]
(
	[IdEnigma] INT IDENTITY,
	[EnigmaName] NVARCHAR(50) NOT NULL,

	CONSTRAINT PK_Enigma PRIMARY KEY(IdEnigma),
)
