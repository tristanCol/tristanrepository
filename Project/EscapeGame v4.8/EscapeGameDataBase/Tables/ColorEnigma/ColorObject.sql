﻿CREATE TABLE [dbo].[ColorObject]
(
	[IdColorObject] INT NOT NULL IDENTITY, 
    [IdEnigma] INT NOT NULL, 
    [Color] NVARCHAR(15) NOT NULL,
	CONSTRAINT PK_IdColorObject PRIMARY KEY(IdColorObject),
	CONSTRAINT FK_IdEnigmaColorObject FOREIGN KEY(IdEnigma) REFERENCES Enigma(IdEnigma)
)
