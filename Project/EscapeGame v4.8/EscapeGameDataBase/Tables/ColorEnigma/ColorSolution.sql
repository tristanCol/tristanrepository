﻿CREATE TABLE [dbo].[ColorSolution]
(
	[IdColorSolution] INT NOT NULL IDENTITY,
	[IdEnigma] INT NOT NULL,
    [Solution] NVARCHAR(150) NOT NULL,
	
	CONSTRAINT PK_IdColorSolution PRIMARY KEY(IdColorSolution),
	CONSTRAINT FK_IdEnigmaColorSolution FOREIGN KEY(IdEnigma) REFERENCES Enigma(IdEnigma)
)
