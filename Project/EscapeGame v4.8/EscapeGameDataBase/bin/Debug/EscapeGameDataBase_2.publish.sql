﻿/*
Script de déploiement pour EscapeGameDB

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "EscapeGameDB"
:setvar DefaultFilePrefix "EscapeGameDB"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'L''opération de refactorisation de changement de nom avec la clé 28c9c1e2-dec2-4d02-88d1-d1cf5de59c18 est ignorée, l''élément [dbo].[BookObject].[Id] (SqlSimpleColumn) ne sera pas renommé en IdBookObject';


GO
PRINT N'L''opération de refactorisation de changement de nom avec la clé 71c79188-f20e-4160-a436-2a6f0125f694, 8658773b-8e3d-49ad-8ffd-8e73d2374e17 est ignorée, l''élément [dbo].[ColorObject].[Id] (SqlSimpleColumn) ne sera pas renommé en IdColorObject';


GO
PRINT N'L''opération de refactorisation de changement de nom avec la clé 93b40025-b3ee-4560-9261-1535d79e46d1 est ignorée, l''élément [dbo].[ColorSolution].[Id] (SqlSimpleColumn) ne sera pas renommé en IdColo';


GO
PRINT N'Suppression de [dbo].[FK_IdEnigma]...';


GO
ALTER TABLE [dbo].[BasketObject] DROP CONSTRAINT [FK_IdEnigma];


GO
PRINT N'Suppression de [dbo].[FK_IdEnigmaSolution]...';


GO
ALTER TABLE [dbo].[BasketSolution] DROP CONSTRAINT [FK_IdEnigmaSolution];


GO
PRINT N'Création de [dbo].[BookObject]...';


GO
CREATE TABLE [dbo].[BookObject] (
    [IdBookObject] INT            IDENTITY (1, 1) NOT NULL,
    [IdEnigma]     INT            NOT NULL,
    [Titre]        NVARCHAR (125) NULL,
    [Auteur]       NVARCHAR (75)  NULL,
    [NbPages]      INT            NULL,
    CONSTRAINT [PK_BookObject] PRIMARY KEY CLUSTERED ([IdBookObject] ASC)
);


GO
PRINT N'Création de [dbo].[BookSolution]...';


GO
CREATE TABLE [dbo].[BookSolution] (
    [IdBookSolution] INT            IDENTITY (1, 1) NOT NULL,
    [IdEnigma]       INT            NOT NULL,
    [Solution]       NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_IdBookSolution] PRIMARY KEY CLUSTERED ([IdBookSolution] ASC)
);


GO
PRINT N'Création de [dbo].[CalendarSolution]...';


GO
CREATE TABLE [dbo].[CalendarSolution] (
    [IdCalendarSolution] INT           IDENTITY (1, 1) NOT NULL,
    [IdEnigma]           INT           NOT NULL,
    [Day]                INT           NOT NULL,
    [Month]              INT           NOT NULL,
    [Year]               INT           NOT NULL,
    [Event]              NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_IdCalendarSolution] PRIMARY KEY CLUSTERED ([IdCalendarSolution] ASC)
);


GO
PRINT N'Création de [dbo].[ColorObject]...';


GO
CREATE TABLE [dbo].[ColorObject] (
    [IdColorObject] INT          IDENTITY (1, 1) NOT NULL,
    [IdEnigma]      INT          NOT NULL,
    [Color]         NVARCHAR (5) NOT NULL,
    CONSTRAINT [PK_IdColorObject] PRIMARY KEY CLUSTERED ([IdColorObject] ASC)
);


GO
PRINT N'Création de [dbo].[ColorSolution]...';


GO
CREATE TABLE [dbo].[ColorSolution] (
    [IdColorSolution] INT           IDENTITY (1, 1) NOT NULL,
    [IdEnigma]        INT           NOT NULL,
    [Solution]        NVARCHAR (25) NOT NULL,
    CONSTRAINT [PK_IdColorSolution] PRIMARY KEY CLUSTERED ([IdColorSolution] ASC)
);


GO
PRINT N'Création de [dbo].[FK_IdEnigmaBookObject]...';


GO
ALTER TABLE [dbo].[BookObject] WITH NOCHECK
    ADD CONSTRAINT [FK_IdEnigmaBookObject] FOREIGN KEY ([IdEnigma]) REFERENCES [dbo].[Enigma] ([IdEnigma]);


GO
PRINT N'Création de [dbo].[FK_IdEnigmaBookSolution]...';


GO
ALTER TABLE [dbo].[BookSolution] WITH NOCHECK
    ADD CONSTRAINT [FK_IdEnigmaBookSolution] FOREIGN KEY ([IdEnigma]) REFERENCES [dbo].[Enigma] ([IdEnigma]);


GO
PRINT N'Création de [dbo].[FK_IdEnigmaCalendarSolution]...';


GO
ALTER TABLE [dbo].[CalendarSolution] WITH NOCHECK
    ADD CONSTRAINT [FK_IdEnigmaCalendarSolution] FOREIGN KEY ([IdEnigma]) REFERENCES [dbo].[Enigma] ([IdEnigma]);


GO
PRINT N'Création de [dbo].[FK_IdEnigmaColorObject]...';


GO
ALTER TABLE [dbo].[ColorObject] WITH NOCHECK
    ADD CONSTRAINT [FK_IdEnigmaColorObject] FOREIGN KEY ([IdEnigma]) REFERENCES [dbo].[Enigma] ([IdEnigma]);


GO
PRINT N'Création de [dbo].[FK_IdEnigmaColorSolution]...';


GO
ALTER TABLE [dbo].[ColorSolution] WITH NOCHECK
    ADD CONSTRAINT [FK_IdEnigmaColorSolution] FOREIGN KEY ([IdEnigma]) REFERENCES [dbo].[Enigma] ([IdEnigma]);


GO
PRINT N'Création de [dbo].[FK_IdEnigmaBasketObject]...';


GO
ALTER TABLE [dbo].[BasketObject] WITH NOCHECK
    ADD CONSTRAINT [FK_IdEnigmaBasketObject] FOREIGN KEY ([IdEnigma]) REFERENCES [dbo].[Enigma] ([IdEnigma]);


GO
PRINT N'Création de [dbo].[FK_IdEnigmaBasketSolution]...';


GO
ALTER TABLE [dbo].[BasketSolution] WITH NOCHECK
    ADD CONSTRAINT [FK_IdEnigmaBasketSolution] FOREIGN KEY ([IdEnigma]) REFERENCES [dbo].[Enigma] ([IdEnigma]);


GO
-- Étape de refactorisation pour mettre à jour le serveur cible avec des journaux de transactions déployés
IF NOT EXISTS (SELECT OperationKey FROM [dbo].[__RefactorLog] WHERE OperationKey = '28c9c1e2-dec2-4d02-88d1-d1cf5de59c18')
INSERT INTO [dbo].[__RefactorLog] (OperationKey) values ('28c9c1e2-dec2-4d02-88d1-d1cf5de59c18')
IF NOT EXISTS (SELECT OperationKey FROM [dbo].[__RefactorLog] WHERE OperationKey = '71c79188-f20e-4160-a436-2a6f0125f694')
INSERT INTO [dbo].[__RefactorLog] (OperationKey) values ('71c79188-f20e-4160-a436-2a6f0125f694')
IF NOT EXISTS (SELECT OperationKey FROM [dbo].[__RefactorLog] WHERE OperationKey = '8658773b-8e3d-49ad-8ffd-8e73d2374e17')
INSERT INTO [dbo].[__RefactorLog] (OperationKey) values ('8658773b-8e3d-49ad-8ffd-8e73d2374e17')
IF NOT EXISTS (SELECT OperationKey FROM [dbo].[__RefactorLog] WHERE OperationKey = '93b40025-b3ee-4560-9261-1535d79e46d1')
INSERT INTO [dbo].[__RefactorLog] (OperationKey) values ('93b40025-b3ee-4560-9261-1535d79e46d1')

GO

GO
PRINT N'Vérification de données existantes par rapport aux nouvelles contraintes';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[BookObject] WITH CHECK CHECK CONSTRAINT [FK_IdEnigmaBookObject];

ALTER TABLE [dbo].[BookSolution] WITH CHECK CHECK CONSTRAINT [FK_IdEnigmaBookSolution];

ALTER TABLE [dbo].[CalendarSolution] WITH CHECK CHECK CONSTRAINT [FK_IdEnigmaCalendarSolution];

ALTER TABLE [dbo].[ColorObject] WITH CHECK CHECK CONSTRAINT [FK_IdEnigmaColorObject];

ALTER TABLE [dbo].[ColorSolution] WITH CHECK CHECK CONSTRAINT [FK_IdEnigmaColorSolution];

ALTER TABLE [dbo].[BasketObject] WITH CHECK CHECK CONSTRAINT [FK_IdEnigmaBasketObject];

ALTER TABLE [dbo].[BasketSolution] WITH CHECK CHECK CONSTRAINT [FK_IdEnigmaBasketSolution];


GO
PRINT N'Mise à jour terminée.';


GO
