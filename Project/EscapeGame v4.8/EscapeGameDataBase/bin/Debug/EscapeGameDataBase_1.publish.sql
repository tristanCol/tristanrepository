﻿/*
Script de déploiement pour EscapeGameDB

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "EscapeGameDB"
:setvar DefaultFilePrefix "EscapeGameDB"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
/*
La colonne [dbo].[BasketObject].[ActualOrder] est en cours de suppression, des données risquent d'être perdues.

La colonne [dbo].[BasketObject].[CorrectOrder] est en cours de suppression, des données risquent d'être perdues.
*/

IF EXISTS (select top 1 1 from [dbo].[BasketObject])
    RAISERROR (N'Lignes détectées. Arrêt de la mise à jour du schéma en raison d''''un risque de perte de données.', 16, 127) WITH NOWAIT

GO
PRINT N'L''opération de refactorisation de changement de nom avec la clé 9107437d-c961-4d9e-964c-2960ed43541a est ignorée, l''élément [dbo].[BasketSolution].[Id] (SqlSimpleColumn) ne sera pas renommé en IdEnigma';


GO
PRINT N'Suppression de [dbo].[FK_IdEnigma]...';


GO
ALTER TABLE [dbo].[BasketObject] DROP CONSTRAINT [FK_IdEnigma];


GO
PRINT N'Début de la régénération de la table [dbo].[BasketObject]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_BasketObject] (
    [IdBasketObject] INT           IDENTITY (1, 1) NOT NULL,
    [IdEnigma]       INT           NOT NULL,
    [Word]           NVARCHAR (26) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_IdBasketObject1] PRIMARY KEY CLUSTERED ([IdBasketObject] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[BasketObject])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_BasketObject] ON;
        INSERT INTO [dbo].[tmp_ms_xx_BasketObject] ([IdBasketObject], [IdEnigma], [Word])
        SELECT   [IdBasketObject],
                 [IdEnigma],
                 [Word]
        FROM     [dbo].[BasketObject]
        ORDER BY [IdBasketObject] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_BasketObject] OFF;
    END

DROP TABLE [dbo].[BasketObject];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_BasketObject]', N'BasketObject';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_IdBasketObject1]', N'PK_IdBasketObject', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Création de [dbo].[BasketSolution]...';


GO
CREATE TABLE [dbo].[BasketSolution] (
    [IdBasketSolution] INT            IDENTITY (1, 1) NOT NULL,
    [IdEnigma]         INT            NOT NULL,
    [Solution]         NVARCHAR (250) NOT NULL,
    CONSTRAINT [PK_IdBasketSolution] PRIMARY KEY CLUSTERED ([IdBasketSolution] ASC)
);


GO
PRINT N'Création de [dbo].[FK_IdEnigma]...';


GO
ALTER TABLE [dbo].[BasketObject] WITH NOCHECK
    ADD CONSTRAINT [FK_IdEnigma] FOREIGN KEY ([IdEnigma]) REFERENCES [dbo].[Enigma] ([IdEnigma]);


GO
PRINT N'Création de [dbo].[FK_IdEnigmaSolution]...';


GO
ALTER TABLE [dbo].[BasketSolution] WITH NOCHECK
    ADD CONSTRAINT [FK_IdEnigmaSolution] FOREIGN KEY ([IdEnigma]) REFERENCES [dbo].[Enigma] ([IdEnigma]);


GO
-- Étape de refactorisation pour mettre à jour le serveur cible avec des journaux de transactions déployés
IF NOT EXISTS (SELECT OperationKey FROM [dbo].[__RefactorLog] WHERE OperationKey = '9107437d-c961-4d9e-964c-2960ed43541a')
INSERT INTO [dbo].[__RefactorLog] (OperationKey) values ('9107437d-c961-4d9e-964c-2960ed43541a')

GO

GO
PRINT N'Vérification de données existantes par rapport aux nouvelles contraintes';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[BasketObject] WITH CHECK CHECK CONSTRAINT [FK_IdEnigma];

ALTER TABLE [dbo].[BasketSolution] WITH CHECK CHECK CONSTRAINT [FK_IdEnigmaSolution];


GO
PRINT N'Mise à jour terminée.';


GO
