﻿using AspRegister.Infrastructures;
using AspRegister.Models;
using AspRegister.Models.ViewModels;
using DAL_EscapeAsp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AspRegister.Controllers
{
    public class ReservationController : Controller
    {
        // GET: Reservation
        [AuthRequired]
        public ActionResult Index(int id)
        {
            ReservationService RService = new ReservationService();
            IEnumerable<Reservation> reservations = RService.Get().Where(w => w.IdUser == id).Select(s => s.ToASP());
            return View(reservations);
        }

        // GET: Reservation/Details/5
        [AuthRequired]
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Reservation/Create
        [AuthRequired]
        public ActionResult Create()
        {
            AddReservationForm form = new AddReservationForm();
            //ScenarioService SService = new ScenarioService();

            //IEnumerable<SelectListItem> scenarioList = SService.Get().Select(r => new SelectListItem { Value = r.Id.ToString(), Text = r.Name });
            //ViewBag.scenarioList = scenarioList;
            return View(form);
        }

        // POST: Reservation/Create
        [AuthRequired]
        [HttpPost]
        public ActionResult Create(AddReservationForm form)
        {
            if (ModelState.IsValid)
            {
                User savedUser = form.SaveToDB();

                return RedirectToAction("Index", "Reservation", new { id = savedUser.Id });
            }
            else
            {
                return View(form);
            }
        }

        // GET: Reservation/Edit/5
        [AuthRequired]
        public ActionResult Edit(int id)
        {
            EditReservationForm form = new EditReservationForm();
            return View(form);
        }

        // POST: Reservation/Edit/5
        [AuthRequired]
        [HttpPost]
        public ActionResult Edit(int id, EditReservationForm form)
        {
            if (ModelState.IsValid)
            {
                Reservation savedRes = form.UpdateDB();

                return RedirectToAction("Index", "Reservation", new { id = savedRes.Id });
            }
            else
            {
                return View(form);
            }
        }

        // GET: Reservation/Delete/5
        [AuthRequired]
        public ActionResult Delete(int id)
        {
            ReservationService RService = new ReservationService();
            RService.Delete(id);

            return RedirectToAction("Index", "Reservation", new { id = UserSession.UserLog.UserLogId });
        }
        
    }
}
