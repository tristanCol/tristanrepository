﻿using AspRegister.Infrastructures;
using AspRegister.Models;
using AspRegister.Models.ViewModels;
using DAL_EscapeAsp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AspRegister.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (UserSession.UserLog != null)
                return RedirectToAction("Details", "Home", new {id = UserSession.UserLog.UserLogId});
            return RedirectToAction("Login", "Home");
        }

        [AuthRequired]
        public ActionResult Details(int id)
        {
            DetailsForm form = new DetailsForm(id);
            return View(form);
        }

        [AnonymousRequired]
        public ActionResult Register()
        {
            RegisterForm form = new RegisterForm();
            return View(form);
        }

        [HttpPost]
        [AnonymousRequired]
        public ActionResult Register(RegisterForm form)
        {
            if (ModelState.IsValid)
            {
                User savedUser = form.SaveToDB();
                UserSession.UserLog = new UserLog
                {
                    UserLogId = savedUser.Id
                };
                return RedirectToAction("Details", "Home", new { id = savedUser.Id });
            }
            else
                ViewBag.msg = "Informations incorrectes";
                return View(form);
        }

        [AnonymousRequired]
        public ActionResult Login()
        {
            LoginForm form = new LoginForm();
            return View(form);
        }

        [HttpPost]
        [AnonymousRequired]
        public ActionResult Login(LoginForm form)
        {
            if (ModelState.IsValid)
            {
                if (form.LoginCheck())
                {
                    UserSession.UserLog = new UserLog() { UserLogId = form.UserId };
                    return RedirectToAction("Details", "Home", new { id = form.UserId });
                }
                ViewBag.msg = "Informations incorrectes";
            }
            return View(form);
        }

        [AuthRequired]
        public ActionResult LogOut()
        {
            HttpContext.Session.Abandon();
            return RedirectToAction("Login", "Home");
        }
        
        [AuthRequired]
        public ActionResult Edit(int id)
        {
            UserService UService = new UserService();
            EditForm form = new EditForm(UService.Get(id).ToASP());
            return View(form);
        }
        
        [HttpPost]
        [AuthRequired]
        public ActionResult Edit(int id, EditForm form)
        {
            if (ModelState.IsValid)
            {
                if (form.UpdateToDB())
                    return RedirectToAction("Details", "Home", new { id });
                else
                    ViewBag.msg = "Informations incorrectes";
            }
            return View(form);
        }

    }
}