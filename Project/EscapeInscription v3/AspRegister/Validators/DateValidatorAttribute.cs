﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AspRegister.Validators
{
    public class DateValidatorAttribute : ValidationAttribute
    {
        
        public override bool IsValid(object value)
        {
            DateTime date = (DateTime)value;
            if (date > DateTime.Now)
            {
                return true;
            }
            return false;
        }

        public DateValidatorAttribute()
        {
            ErrorMessage = "La date ne peut être inférieure à la date du jour.";
        }
    }
}