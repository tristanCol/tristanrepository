﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AspRegister.Validators
{
    public class TimeValidatorAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            int h = ((DateTimeOffset)value).Hour;
            if (h <= 19 && h >= 8)
            {
                return true;
            }
            return false;
        }

        public TimeValidatorAttribute()
        {
            ErrorMessage = "Les heures d'ouvertures sont de 8h00 à 19h00.";
        }
    }
}