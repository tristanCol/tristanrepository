﻿using AspRegister.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AspRegister.Infrastructures
{
    public class UserSession
    {
        public static UserLog UserLog
        {
            get => (UserLog)HttpContext.Current.Session["UserLog"];
            set => HttpContext.Current.Session["UserLog"] = value;
        }
    }
}