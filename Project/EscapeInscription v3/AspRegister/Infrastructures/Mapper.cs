﻿using ASP = AspRegister.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DA = DAL_EscapeAsp.Data;
using DAL_EscapeAsp.Services;

namespace AspRegister.Infrastructures
{
    public static class Mapper
    {
        public static ASP.User ToASP(this DA.User user)
        {
            return (user == null) ? null : new ASP.User()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Password = user.Password
            };
        }

        public static DA.User ToDAL(this ASP.User user)
        {
            return (user == null) ? null : new DA.User()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Password = user.Password
            };
        }

        public static ASP.Reservation ToASP(this DA.Reservation reserv)
        {

            UserService UService = new UserService();
            ScenarioService SService = new ScenarioService();
            return (reserv == null) ? null : new ASP.Reservation()
            {
                Id = reserv.Id,
                User = UService.Get(reserv.IdUser).ToASP(),
                Scenario = new ASP.Scenario()
                {
                    Id = reserv.IdScenario,
                    Name = SService.Get(reserv.IdScenario).Name
                },
                Date = reserv.Date
            };
        }
        public static DA.Reservation ToDAL(this ASP.Reservation reserv)
        {
            return (reserv == null) ? null : new DA.Reservation()
            {
                Id = reserv.Id,
                IdScenario = reserv.Scenario.Id,
                IdUser = reserv.User.Id,
                Date = new DateTime(reserv.Date.Year, reserv.Date.Month, reserv.Date.Day, reserv.Time.Hour, reserv.Time.Minute, 0)
            };
        }

    }
}