﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace AspRegister.Models
{
    public class Reservation
    {
        [ReadOnly(true)]
        public int Id { get; set; }
        public User User { get; set; }
        public Scenario Scenario { get; set; }
        public DateTime Date { get; set; }
        public DateTimeOffset Time { get; set; }
    }
}