﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace AspRegister.Models
{
    public class Scenario
    {
        [ReadOnly(true)]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}