﻿using DAL_EscapeAsp.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DA = DAL_EscapeAsp.Data;

namespace AspRegister.Models.ViewModels
{
    public class LoginForm
    {
        [Required]
        [Display(Name = "Email")]
        public string UserEmail { get; set; }

        [Required]
        [Display(Name = "Mot de passe")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public int UserId
        {
            get
            {
                UserService UService = new UserService();
                int userId = UService.Get().Where(w => w.Email == UserEmail).Select(s => s.Id).SingleOrDefault();
                return userId;
            }
        }


        public bool LoginCheck()
        {
            UserService UService = new UserService();
            DA.User user = UService.Get(UserId);
            if (user != null && (user.Email == UserEmail && user.Password == Password))
                return true;
            return false;
        }
    }
}