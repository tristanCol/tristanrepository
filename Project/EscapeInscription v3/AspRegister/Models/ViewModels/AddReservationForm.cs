﻿using AspRegister.Infrastructures;
using AspRegister.Validators;
using DAL_EscapeAsp.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AspRegister.Models.ViewModels
{
    public class AddReservationForm
    {
        private Reservation _Reservation;

        [Required]
        [Display(Name = "Scénario")]
        public int ScenarioID
        {
            get { return (_Reservation.Scenario==null)?0:_Reservation.Scenario.Id; }
            set
            {
                ScenarioService SS = new ScenarioService();
                DAL_EscapeAsp.Data.Scenario SSResult = SS.Get(value);
                _Reservation.Scenario = new Scenario() { Id = SSResult.Id, Name = SSResult.Name };
            }
        }

        public IEnumerable<SelectListItem> Items {
            get {
                    ScenarioService SS = new ScenarioService();
                    return SS.Get().Select(s => new SelectListItem() { Value=s.Id.ToString(), Text=s.Name});
                    }
        }

        [Required]
        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        [DateValidator]
        public DateTime Date
        {
            get => _Reservation.Date;
            set => _Reservation.Date = value;
        }

        [Required]
        [Display(Name = "Heure")]
        [DataType(DataType.Time)]
        [TimeValidator]
        public DateTimeOffset Time
        {
            get => _Reservation.Time;
            set => _Reservation.Time = value;
        }

        public AddReservationForm()
        {
            _Reservation = new Reservation();
            UserService UService = new UserService();
            _Reservation.User = UService.Get(UserSession.UserLog.UserLogId).ToASP();
        }

        public User SaveToDB()
        {
            UserService UService = new UserService();
            DateTime dateH = new DateTime(_Reservation.Date.Year, _Reservation.Date.Month, _Reservation.Date.Day, _Reservation.Time.Hour, _Reservation.Time.Minute, 0);

            ReservationService RService = new ReservationService();
            _Reservation.Id = RService.Insert(new DAL_EscapeAsp.Data.Reservation()
            {
                IdScenario = _Reservation.Scenario.Id,
                IdUser = _Reservation.User.Id,
                Date = dateH
            });
            return UService.Get(_Reservation.User.Id).ToASP();
        }
    }
}