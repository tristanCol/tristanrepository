﻿using AspRegister.Infrastructures;
using DAL_EscapeAsp.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AspRegister.Models.ViewModels
{
    public class DetailsForm
    {
        private User _User;

        [HiddenInput(DisplayValue = false)]
        public int Id { get => _User.Id; }

        [Display(Name = "Nom")]
        public string LastName { get => _User.LastName; }

        [Display(Name = "Prénom")]
        public string FirstName { get => _User.FirstName; }

        [Display(Name = "Email")]
        public string Email { get => _User.Email; }

        [Display(Name = "Mot de passe")]
        [DataType(DataType.Password)]
        public string Password { get => _User.Password; }

        public DetailsForm()
        {
        }

        public DetailsForm(int id)
        {
            UserService UService = new UserService();
            _User = UService.Get(id).ToASP();
        }
    }
}