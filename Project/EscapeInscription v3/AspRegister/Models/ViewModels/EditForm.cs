﻿using AspRegister.Infrastructures;
using DAL_EscapeAsp.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL_EscapeAsp.Services;

namespace AspRegister.Models.ViewModels
{
    public class EditForm
    {
        private User _User;

        [Required]
        [HiddenInput(DisplayValue = false)]
        public int UserId { get => _User.Id; set => _User.Id = value; }

        [Display(Name = "Prénom")]
        [Required]
        [StringLength(16, MinimumLength = 3)]
        public string FirstName
        {
            get => _User.FirstName;
            set => _User.FirstName = value;
        }

        [Display(Name = "Nom")]
        [Required]
        [StringLength(16, MinimumLength = 3)]
        public string LastName
        {
            get => _User.LastName;
            set => _User.LastName = value;
        }

        [Required]
        [Display(Name = "Adresse e-mail")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}")]
        public string Email { get => _User.Email; set => _User.Email = value; }

        [Required]
        [Display(Name = "Mot de passe actuel")]
        [DataType(DataType.Password)]
        public string ActualPwd { get; set; }

        private string _HiddenPwd;

        [Required]
        [Display(Name = "Nouveau mot de passe", Description = "Votre mot de passe doit contenir au minimum 8 caractères et être composé d'une majuscule et d'un chiffre.")]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,20}$", ErrorMessage = "Votre mot de passe doit contenir au minimum 6 caractères et être composé d'une majuscule et d'un chiffre.")]
        [DataType(DataType.Password)]
        public string Pwd { get => _HiddenPwd; set => _User.Password = value; }

        [Required]
        [Display(Name = "Confirmation du nouveau mot de passe")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare(nameof(Pwd), ErrorMessage = "Le mot de passe ne correspond pas")]
        public string PwdCheck { get { return _HiddenPwd; } set {_HiddenPwd = value ; } }

        public EditForm()
        {
            _User = new User();
        }

        public EditForm(User user)
        {
            _User = user;
        }

        public bool UpdateToDB()
        {
            UserService UService = new UserService();

            if (ActualPwd == UService.Get(UserSession.UserLog.UserLogId).Password)
                return UService.Update(_User.ToDAL());
            return false;
        }
    }
}