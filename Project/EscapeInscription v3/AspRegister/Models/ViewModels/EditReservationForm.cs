﻿using AspRegister.Infrastructures;
using DAL_EscapeAsp.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AspRegister.Models.ViewModels
{
    public class EditReservationForm
    {
        private Reservation _Reservation;

        [Required]
        [HiddenInput]
        public int ScenarioID
        {
            get { return (_Reservation.Scenario == null) ? 0 : _Reservation.Scenario.Id; }
            set
            {
                ScenarioService SS = new ScenarioService();
                DAL_EscapeAsp.Data.Scenario SSResult = SS.Get(value);
                _Reservation.Scenario = new Scenario() { Id = SSResult.Id, Name = SSResult.Name };
            }
        }

        public IEnumerable<SelectListItem> Items
        {
            get
            {
                ScenarioService SS = new ScenarioService();
                return SS.Get().Select(s => new SelectListItem() { Value = s.Id.ToString(), Text = s.Name });
            }
        }

        [Required]
        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        public DateTime Date
        {
            get => _Reservation.Date;
            set => _Reservation.Date = value;
        }

        [Required]
        [Display(Name = "Heure")]
        [DataType(DataType.Time)]
        public DateTimeOffset Time
        {
            get => _Reservation.Time;
            set => _Reservation.Time = value;
        }

        public EditReservationForm()
        {
            _Reservation = new Reservation();
            UserService UService = new UserService();
            _Reservation.User = UService.Get(UserSession.UserLog.UserLogId).ToASP();
        }

        public Reservation UpdateDB()
        {
            ReservationService RService = new ReservationService();

            if (RService.Update(_Reservation.ToDAL()))
                return RService.Get(_Reservation.Id).ToASP();
            throw new NotImplementedException("Fail update");
        }
    }
}