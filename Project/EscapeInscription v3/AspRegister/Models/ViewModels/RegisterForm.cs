﻿using DAL_EscapeAsp.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AspRegister.Models.ViewModels
{
    public class RegisterForm
    {
        private User _User;

        [Display(Name = "Prénom")]
        [Required]
        public string FirstName
        {
            get => _User.FirstName;
            set => _User.FirstName = value;
        }

        [Display(Name = "Nom")]
        [Required]
        public string LastName
        {
            get => _User.LastName;
            set => _User.LastName = value;
        }

        [Required]
        [Display(Name = "Adresse e-mail")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Adresse e-mail incorrect")]
        public string Email
        {
            get => _User.Email;
            set => _User.Email = value;
        }

        [Required]
        [Display(Name = "Mot de passe", Description = "Votre mot de passe doit contenir au minimum 6 caractères et être composé d'une majuscule et d'un chiffre.")]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,20}$", ErrorMessage = "Votre mot de passe doit contenir au minimum 6 caractères et être composé d'une majuscule et d'un chiffre.")]
        [DataType(DataType.Password)]
        public string Password
        {
            get => _User.Password;
            set => _User.Password = value;
        }

        [Required]
        [Display(Name = "Confirmation du mot de passe")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare(nameof(Password), ErrorMessage = "Le mot de passe ne correspond pas")]
        public string PwdCheck { get; set; }

        public RegisterForm()
        {
            _User = new User();
        }

        public User SaveToDB()
        {
            UserService UService = new UserService();
            _User.Id = UService.Insert(new DAL_EscapeAsp.Data.User()
            {
                Email = _User.Email,
                Password = _User.Password,
                FirstName = _User.FirstName,
                LastName = _User.LastName,
                IsActive = true
            });
            return _User;
        }
    }
}