﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AspRegister.Models
{
    public class UserLog
    {
        public int UserLogId { get; set; }

        public Dictionary<string, bool> HaveRights { get; set; }

        public void GiveAdminRight()
        {
            HaveRights["IsAdmin"] = true;
        }
    }
}