﻿using DAL_EscapeAsp.Data;
using DAL_EscapeAsp.Mapper;
using DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_EscapeAsp.Services
{
    public class ScenarioService : IRepository<Scenario, int>
    {
        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Scenario> Get()
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM Scenario", false);
            return connection.ExecuteReader<Scenario>(command, reader => reader.ToScenario());
        }

        public Scenario Get(int id)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM Scenario WHERE Id = @id", false);
            command.AddParameter("id", id);
            return connection.ExecuteReader<Scenario>(command, reader => reader.ToScenario()).SingleOrDefault();
        }

        public int Insert(Scenario entity)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("INSERT INTO Scenario (Name) " +
                "OUTPUT inserted.Id values (@Name)", false);

            command.AddParameter("Name", entity.Name);
            return (int)connection.ExecuteScalar(command);
        }

        public bool Update(Scenario entity)
        {
            throw new NotImplementedException();
        }
    }
}
