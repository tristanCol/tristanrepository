﻿using DAL_EscapeAsp.Data;
using DAL_EscapeAsp.Mapper;
using DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_EscapeAsp.Services
{
    public class UserService : IRepository<User, int>
    {
        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> Get()
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM [User]", false);
            return connection.ExecuteReader<User>(command, reader => reader.ToUser());
        }

        public User Get(int id)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM [User] WHERE Id = @id", false);
            command.AddParameter("id", id);
            return connection.ExecuteReader<User>(command, reader => reader.ToUser()).SingleOrDefault();
        }

        public int Insert(User entity)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("INSERT INTO [User] (Email, Password, FirstName, LastName) " +
                "OUTPUT inserted.Id VALUES (@Email, @Password, @FirstName, @LastName)", false);

            command.AddParameter("Email", entity.Email);
            command.AddParameter("Password", entity.Password);
            command.AddParameter("FirstName", entity.FirstName);
            command.AddParameter("LastName", entity.LastName);
            return (int)connection.ExecuteScalar(command);
        }

        public bool Update(User entity)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("UPDATE [User] SET LastName = @LastName, FirstName = @FirstName, Email = @Email, Password = @Password WHERE Id = @Id", false);

            command.AddParameter("LastName", entity.LastName);
            command.AddParameter("FirstName", entity.FirstName);
            command.AddParameter("Email", entity.Email);
            command.AddParameter("Password", entity.Password);
            command.AddParameter("Id", entity.Id);
           
            return connection.ExecuteNonQuery(command) > 0;
        }
    }
}
