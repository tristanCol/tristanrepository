﻿using DAL_EscapeAsp.Data;
using DAL_EscapeAsp.Mapper;
using DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_EscapeAsp.Services
{
    public class ReservationService : IRepository<Reservation, int>
    {
        public bool Delete(int id)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("DELETE FROM Reservation WHERE Id = @Id", false);
            command.AddParameter("Id", id);
            return connection.ExecuteNonQuery(command) > 0;
        }

        public IEnumerable<Reservation> Get()
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM Reservation", false);
            return connection.ExecuteReader<Reservation>(command, reader => reader.ToReservation());
        }

        public Reservation Get(int id)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("SELECT * FROM Reservation WHERE Id = @Id", false);
            command.AddParameter("Id", id);
            return connection.ExecuteReader<Reservation>(command, reader => reader.ToReservation()).SingleOrDefault();
        }

        public int Insert(Reservation entity)
        {
            Connection connection = new Connection(DBConfig.CONNSTRING);
            Command command = new Command("INSERT INTO Reservation (IdScenario, IdUser, Date) " +
                "OUTPUT inserted.Id values (@IdScenario, @IdUser, @Date)", false);

            command.AddParameter("IdScenario", entity.IdScenario);
            command.AddParameter("IdUser", entity.IdUser);
            command.AddParameter("Date", entity.Date);
            return (int)connection.ExecuteScalar(command);
        }

        public bool Update(Reservation entity)
        {
            throw new NotImplementedException();
        }
    }
}
