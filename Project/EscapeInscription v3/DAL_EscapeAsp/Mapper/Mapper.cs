﻿using DAL_EscapeAsp.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_EscapeAsp.Mapper
{
    public static class Mapper
    {
        public static User ToUser(this IDataRecord record) => new User()
        {
            Id = (int)record["Id"],
            Email = (string)record["Email"],
            Password = (string)record["Password"],
            FirstName = (string)record["FirstName"],
            LastName = (string)record["LastName"],
            IsActive = (bool)record["IsActive"]
        };

        public static Scenario ToScenario(this IDataRecord record) => new Scenario()
        {
            Id = (int)record["Id"],
            Name = (string)record["Name"]
        };

        public static Reservation ToReservation(this IDataRecord record) => new Reservation()
        {
            Id = (int)record["Id"],
            IdUser = (int)record["IdUser"],
            IdScenario = (int)record["IdScenario"],
            Date = (DateTime)record["Date"]
        };
    }
}
