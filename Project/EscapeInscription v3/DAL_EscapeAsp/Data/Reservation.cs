﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_EscapeAsp.Data
{
    public class Reservation
    {
        public int Id { get; set; }
        public int IdUser { get; set; }
        public int IdScenario { get; set; }
        public DateTime Date { get; set; }
    }
}
