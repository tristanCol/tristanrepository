﻿CREATE TABLE [dbo].[User]
(
	[Id] INT IDENTITY,
    [Email] NVARCHAR(254) NOT NULL, 
    [Password] NVARCHAR(16) NOT NULL, 
    [FirstName] NVARCHAR(50) NOT NULL, 
    [LastName] NVARCHAR(50) NOT NULL, 
    [IsActive] BIT NOT NULL

	CONSTRAINT PK_User PRIMARY KEY(Id)
)
