﻿CREATE TABLE [dbo].[Reservation]
(
	[Id] INT IDENTITY,
    [IdUser] INT NOT NULL, 
    [IdScenario] INT NOT NULL, 
    [Date] DATETIME2 NOT NULL

	CONSTRAINT PK_Reservation PRIMARY KEY(Id),
	CONSTRAINT FK_IdUserReservation FOREIGN KEY(IdUser) REFERENCES [User](Id),
	CONSTRAINT FK_IdScenario FOREIGN KEY(IdScenario) REFERENCES Scenario(Id)
)
