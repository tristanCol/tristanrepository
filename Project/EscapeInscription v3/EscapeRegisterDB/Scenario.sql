﻿CREATE TABLE [dbo].[Scenario]
(
	[Id] INT IDENTITY, 
    [Name] NVARCHAR(50) NOT NULL

	CONSTRAINT PK_Scenario PRIMARY KEY(Id)
)
