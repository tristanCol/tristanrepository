window.addEventListener('load', init);

// Globals

// Available Levels
const levels = {
  easy: 7,
  medium: 5,
  hard: 3,
  impossible: 2,
  hacker:1
};

// Current level
let currentLevel = levels.medium;

let time = currentLevel;
let score = 0;
let isPlaying;

// DOM Elements
const wordInput = document.querySelector('#word-input');
const currentWord = document.querySelector('#current-word');
const scoreDisplay = document.querySelector('#score');
const timeDisplay = document.querySelector('#time');
const message = document.querySelector('#message');
const seconds = document.querySelector('#seconds');
const divSelectsLvl = document.querySelector("#selectsLvl");

// To change lvl to EASY
const btnEasy = divSelectsLvl.querySelector("#easy");

btnEasy.addEventListener("click", () => {
  currentLevel = levels.easy;
  console.log(currentLevel);
  init2();
});

// To change lvl to MEDIUM
const btnMedium = divSelectsLvl.querySelector("#medium");

btnMedium.addEventListener("click", () => {
  currentLevel = levels.medium;
  console.log(currentLevel);
  init2();
});

// To change lvl to HARD
const btnHard = divSelectsLvl.querySelector("#hard");

btnHard.addEventListener("click", () => {
  currentLevel = levels.hard;
  console.log(currentLevel);
  init2();
});

// To change lvl to IMPOSSIBLE
const btnImpossible = divSelectsLvl.querySelector("#impossible");

btnImpossible.addEventListener("click", () => {
  currentLevel = levels.impossible;
  console.log(currentLevel);
  init2();
});




// To change lvl to Hacker
const btnHacker = divSelectsLvl.querySelector("#hacker");

btnHacker.addEventListener("click", () => {
  currentLevel = levels.hacker;
  console.log(currentLevel);
  init2();
});


const words = [
  'escape',
  'game',
  'fear',
  'lucky',
  'hacker',
  'cheater',
  'grey',
  'black',
  'white',
  'friends',
  'coding',
  'typing',
  'javascript',
  'dotnet',
  'virus',
  'echo',
  'hardcore',
  'investigate',
  'horror',
  'symptom',
  'easteregg',
  'magic',
  'master',
  'space',
  'green',
  'developer',
  'net',
  'website',
  'lol',
  'html',
  'css',
  'room',
  'hat',
  'script',
  'program',
  'os',
  'java'
];

// Initialize Game
function init() {
  // Show number of seconds in UI
  seconds.innerHTML = currentLevel;
  // Load word from array
  showWord(words);
  // Start matching on word input
  wordInput.addEventListener('input', startMatch);
  // Call countdown every second
  setInterval(countdown, 1000);
  // Check game status
  setInterval(checkStatus, 50);

  // lock & unlock the btnHacker
  btnHacker.setAttribute("Disabled", true);
}

// Initialize Game after choosing a lvl
function init2() {
  // Show number of seconds in UI
  seconds.innerHTML = currentLevel;
  // Load word from array
  showWord(words);
  // Start matching on word input
  wordInput.addEventListener('input', startMatch);
}

// Start match
function startMatch() {
  if (matchWords()) {
    isPlaying = true;
    time = currentLevel + 1;
    
    if (currentWord.innerText.length < 4){
      score++;
    } 
    else if (currentWord.innerText.length < 6){
      score += 2;
    } 
    else if (currentWord.innerText.length < 8){
      score += 3;
    }
    else{
      score += 4;
    }
    
    if (score >= 100){
      btnHacker.removeAttribute("Disabled");
    };

    showWord(words);

    if (currentLevel !== levels.hacker) wordInput.value = '';
  }

  // If score is -1, display 0
  if (score === -1) {
    scoreDisplay.innerHTML = 0;
  } 
  else {
    scoreDisplay.innerHTML = score;
  }
}

// Match currentWord to wordInput
function matchWords() {
  if (wordInput.value === currentWord.innerHTML) {
    message.innerHTML = 'Correct!!!';
    return true;
  } 
  else {
    message.innerHTML = '';
    return false;
  }
}

// Pick & show random word
function showWord(words) {
  // Generate random array index
  const randIndex = Math.floor(Math.random() * words.length);
  // Output random word
  currentWord.innerHTML = words[randIndex];
  if (currentLevel === levels.hacker) 
  {
    wordInput.value = currentWord.innerHTML;
    wordInput.value += "←";
  }

}

// Countdown timer
function countdown() {
  // Make sure time is not run out
  if (time > 0) {
    // Decrement
    time--;
  } 
  else if (time === 0) {
    // Game is over
    isPlaying = false;
  }
  // Show time
  timeDisplay.innerHTML = time;
}

// Check game status
function checkStatus() {
  if (!isPlaying && time === 0) {
    message.innerHTML = 'Game Over!!!';
    score = -1;
  }
}