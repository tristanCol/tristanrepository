function showTime(){
    var date = new Date();
    var h = date.getHours(); //0 - 23
    var m = date.getMinutes(); // 0 - 59
    var s = date.getSeconds(); // 0 - 59

    var day = "blabla";
    var DAY = date.getDay();
    var D = date.getDate();
    var M = date.getMonth()+1;
    var Y = date.getFullYear();

    var clock = document.getElementById("MyClockDisplay");
    var theDay = document.getElementById("MyDdayDisplay");


    if(DAY == 0) {day = "Dim"}
    else if(DAY == 1) {day = "Lun"}
    else if(DAY == 2) {day = "Mar"}
    else if(DAY == 3) {day = "Mer"}
    else if(DAY == 4) {day = "Jeu"}
    else if(DAY == 5) {day = "Ven"}
    else if(DAY == 6) {day = "Sam"}

    
    h = (h < 10) ? "0" + h : h;
    m = (m < 10) ? "0" + m : m;
    s = (s < 10) ? "0" + s : s;

    if (M > 12) {M = 1}  

    D = (D < 10) ? "0" + D : D;
    M = (M < 10) ? "0" + M : M;
    Y = (Y < 10) ? "0" + Y : Y;

    if(s == 0) {
        clock.style.color = "gold";
        theDay.style.color = "gold";
        clock.style.fontSize= "28.5px";
        theDay.style.fontSize= "18.5px";
    }
    else if(s > 0 && s < 30) {
        clock.style.color = "rgba(255,255,255,0.5)";
        theDay.style.color = "rgba(255,255,255,0.5)";
        clock.style.fontSize= "28px";
        theDay.style.fontSize= "18px";

    }
    else if(s == 30) {
        clock.style.color = "rgb(196, 10, 10)";
        theDay.style.color = "rgb(196, 10, 10)";
        clock.style.fontSize= "28.5px";
        theDay.style.fontSize= "18.5px";

    }
    else{
        clock.style.color = "rgba(255,255,255,0.5)";
        theDay.style.color = "rgba(255,255,255,0.5)";
        clock.style.fontSize= "28px";
        theDay.style.fontSize= "18px";
    }
    

    var time = h + ":" + m + ":" + s;
    clock.innerHTML = time;
    clock.textContent = time;
    
    var Dday = day + ": " + D + "-" + M + "-" + Y ;
    theDay.innerText = Dday;
    theDay.textContent = Dday;


    
    setTimeout(showTime, 1000);
}
setInterval(showTime, 150)

// showTime();