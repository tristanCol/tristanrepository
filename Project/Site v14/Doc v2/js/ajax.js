const URL_API = "http://api.openweathermap.org/data/2.5/weather?";
const URL_API2 = "&mode=xml&units=metric&lang=fr";
const URL_APPID = "&appid=d5c51d2fdda2c7d0a80148ee72379677";


function getXMLHttpRequest(){
    let xhr;
    if(window.XMLHttpRequest)
    {
        xhr = new XMLHttpRequest();
    }
    else
    {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
    return xhr;
}

// methode pour prendre la ville de l'input
function getCity()
{
    if(document.getElementById("city").value.length > 1)  return document.getElementById("city").value;
            else return "Arlon";
}


// au chargement de la page 
window.onload=function(){
    var btn = document.getElementById('btnsubmit');
    console.log("Tapez 'weather.html' dans la barre de recherche pour acceder à la page cachée");

    btn.onclick=function(event){
        event.preventDefault();

        var xhr = getXMLHttpRequest();
        //alert(xhr);
        xhr.open("GET", URL_API+"q="+getCity()+URL_API2+URL_APPID, false);
        xhr.send();

        var xdoc = xhr.responseXML;
        console.log(xdoc);


        // Declaration et affectation des variables
        var city = xdoc.getElementsByTagName("city")[0].getAttribute("name");
        var country = xdoc.getElementsByTagName("country")[0].textContent + "<br />" // pour les initiales il faut un text content car c'est un text "dur" (entre deux balises)

        var temp = xdoc.getElementsByTagName("temperature")[0].getAttribute("value") + " °C" + "&nbsp;"+ "&nbsp;" +" T° actuelle" + "<br />";
        var tempmin = xdoc.getElementsByTagName("temperature")[0].getAttribute("min") + " °C" + "&nbsp;"+ "&nbsp;" + " T° min" + "<br />";
        var tempmax = xdoc.getElementsByTagName("temperature")[0].getAttribute("max") + " °C" + "&nbsp;"+ "&nbsp;" + " T° max" + "<br />";

        
        if (xdoc.getElementsByTagName("coord")[0].getAttribute("lon") < 0){
            var lonlon = xdoc.getElementsByTagName("coord")[0].getAttribute("lon") + " W" +"<br />";
        }
        else{
            var lonlon = xdoc.getElementsByTagName("coord")[0].getAttribute("lon") + " E" +"<br />";
        }
        if (xdoc.getElementsByTagName("coord")[0].getAttribute("lat") < 0){
            var latlat = xdoc.getElementsByTagName("coord")[0].getAttribute("lat") + " S" +"<br />";
        }
        else{
            var latlat = xdoc.getElementsByTagName("coord")[0].getAttribute("lat") + " N" +"<br />";
        }


        // Affichage
        document.getElementById("cityname").innerHTML = city + " | " + country;
        document.getElementById("temp").innerHTML = temp;
        document.getElementById("min").innerHTML = tempmin;
        document.getElementById("max").innerHTML = tempmax;
        document.getElementById("lon").innerHTML = lonlon;
        document.getElementById("lat").innerHTML = latlat;
    }
}


