window.addEventListener("load",function() {

    const text = document.getElementById("nicoS");
    const btn = document.getElementById("nicoB");

    console.log("Tapez 'escape word' dans le champ 'easteregg' pour acceder à la page cachée");

    btn.setAttribute("disabled",true);
    text.value = "";

    function click() {

        if(text.value == "escape word"){
            btn.removeAttribute("Disabled");
        }
        else if(text.value != "escape word"){
            btn.setAttribute("disabled",true);
            //text.value = ""
        }

    }

    function clear() {
        text.value = "";
        btn.setAttribute("disabled",true);
    }

    text.addEventListener("keyup", click);
    btn.addEventListener("click", clear);
    

});